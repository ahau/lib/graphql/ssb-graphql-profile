import AhauClient from 'ahau-graphql-client'

const Server = require('scuttle-testbot')
const { replicate } = require('scuttle-testbot')

const ahauServer = require('ahau-graphql-server')
const fetch = require('node-fetch')
const crypto = require('crypto')

const shs = crypto.randomBytes(32)

module.exports = async function (opts = {}) {
  opts = {
    // for this testbot
    loadContext: false,
    recpsGuard: false,
    isPataka: false,

    // scuttle-testbot
    noDefaultUse: true,

    // general ssb
    caps: { shs },
    ...opts,
    box2: {
      legacyMode: true,
      ...opts.box2
    }
  }
  if (opts.loadContext) { opts.recpsGuard = true }
  if (opts.recpsGuard === true) {
    opts.recpsGuard = {
      allowedTypes: ['profile/pataka', 'contact', 'pub']
    }
  }

  let stack = Server // eslint-disable-line
    .use(require('ssb-db2/core'))
    .use(require('ssb-classic'))
    .use(require('ssb-db2/compat/publish'))
    .use(require('ssb-db2/compat/db'))
    .use(require('ssb-db2/compat/ebt'))
    .use(require('ssb-db2/compat/log-stream'))
    .use(require('ssb-db2/compat/history-stream'))
    .use(require('ssb-db2/compat/post'))
    .use(require('ssb-db2/compat/feedstate'))

    /* for @ssb-graphql/main */
    .use(require('ssb-blobs'))

    /* for testing patakak blocking */
    .use(require('ssb-conn'))
    .use(require('ssb-invite'))
    .use(require('ssb-replicate'))
    .use(require('ssb-friends'))

    /* for @ssb-graphql/profile */
    .use(require('ssb-profile'))

    .use(require('ssb-settings'))

  // required for loadContext atm
  if (!opts.isPataka) {
    stack.use(require('ssb-box2'))
    stack.use(require('ssb-tribes'))
  }

  if (opts.recpsGuard) stack.use(require('ssb-recps-guard'))

  const port = 3000 + Math.random() * 7000 | 0
  opts.name = opts.name + '-' + port

  const ssb = stack(opts)

  const main = require('@ssb-graphql/main')(ssb, { type: opts.isPataka ? 'pataka' : 'person' })
  const context = opts.loadContext
    ? await main.loadContext()
    : {}

  const httpServer = await ahauServer({
    schemas: [
      main,
      require('../')(ssb) // profile
    ],
    context,
    port
  })
  ssb.close.hook((close, args) => {
    httpServer.close()
    close(...args)
  })

  const apollo = new AhauClient(port, { isTesting: true, fetch })

  return {
    id: ssb.id,
    name: opts.name,
    ssb,
    apollo,
    replicate: async (to) => {
      return replicate({
        from: ssb,
        to: to.ssb ? to.ssb : to,
        name: id => id === ssb.id ? opts.name : to.name,
        log: false
      })
    },
    close: (...args) => ssb.close(...args)
  }
}

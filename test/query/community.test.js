const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')
const { GetCommunity } = require('../lib/helpers')

test('community', async t => {
  const { ssb, apollo } = await TestBot()

  const getCommunity = GetCommunity(apollo)

  // create a publicPerson
  const feedId = ssb.id
  const profileId = await p(ssb.profile.community.public.create)({
    preferredName: 'ziva',
    authors: {
      add: [feedId]
    }
  })

  // link profile to a groupId
  const groupId = '%A9OUzXtv7BhaAfSMqBzOO6JC8kvwmZWGVxHDAlM+/so=.cloaked'
  await p(ssb.profile.link.create)({ type: 'group-profile', group: groupId, profile: profileId })

  // set up listener to see how many times db is hit
  let callCount = 0
  ssb.profile.community.public.get.hook((fn, args) => {
    if (args[0] === profileId) callCount++
    fn(...args)
  })

  /* get by profileId */
  const profileRes = await getCommunity(profileId)
  const profileByProfileId = profileRes.data.community
  t.equal(callCount, 1, 'initial profile.get call')

  /* get by groupId */
  const groupRes = await getCommunity(groupId)
  const profileByGroupId = groupRes.data.community
  t.deepEqual(profileByGroupId, profileByProfileId, 'both queries returns the same community profile')
  t.equal(callCount, 1, 'cache was used')

  ssb.close()
  t.end()
})

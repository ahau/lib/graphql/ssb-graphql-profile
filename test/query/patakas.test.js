const test = require('tape')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')
const pull = require('pull-stream')

const TestBot = require('../test-bot')

function Run (t) {
  return function (msg, promise) {
    return promise
      .then(res => {
        t.pass(msg)

        return res.data
          ? Object.values(res.data)[0]
          : res
      })
      .catch(err => t.error(err, msg))
  }
}

test('patakas', async t => {
  const [pataka, alice] = await Promise.all([
    TestBot({
      name: 'pataka',
      isPataka: true,
      loadContext: true,

      host: '127.0.0.1',
      port: 8000 + Math.floor(Math.random() * 1000),
      allowPrivate: true
      // fixes error: no address available for creating an invite, configuration needed for server.
    }),
    TestBot({
      name: 'alice'
    })
  ])

  const run = Run(t)

  const [profileId] = await pull(
    pataka.ssb.createHistoryStream({ id: pataka.ssb.id, limit: 1 }),
    pull.map(m => m.key),
    pull.collectAsPromise()
  )

  await run(
    'pataka profile updated',
    p(pataka.ssb.profile.pataka.public.update)(
      profileId,
      { preferredName: 'Alice family pataka' }
    )
  )

  const invite = await run(
    'invite created',
    p(pataka.ssb.invite.create)({ uses: 10 })
  )

  await run(
    'invite used: ' + invite,
    p(alice.ssb.invite.accept)(invite)
  )

  await replicate({ from: pataka.ssb, to: alice.ssb })

  let patakas = await run(
    'query: patakas',
    alice.apollo.query({
      query: `query {
        patakas {
          id
          feedId
          preferredName
          isBlocked
        }
      }`
    })
  )
  t.deepEqual(
    patakas,
    [{
      id: profileId,
      feedId: pataka.ssb.id,
      preferredName: 'Alice family pataka',
      isBlocked: false
    }]
  )

  await run(
    'alice blocks pataka',
    p(alice.ssb.friends.block)(pataka.ssb.id, {})
  )

  patakas = await run(
    'query: patakas',
    alice.apollo.query({
      query: `query {
        patakas {
          id
          feedId
          preferredName
          isBlocked
        }
      }`
    })
  )
  t.deepEqual(
    patakas,
    [{
      id: profileId,
      feedId: pataka.ssb.id,
      preferredName: 'Alice family pataka',
      isBlocked: true
    }]
  )

  await p(setTimeout)(100)
  await p(pataka.close)()
  await p(alice.close)()
  await p(setTimeout)(100)

  t.end()
})

const test = require('tape')
const { isMsg } = require('ssb-ref')
const { promisify: p } = require('util')
const { replicate } = require('scuttle-testbot')

const TestBot = require('../test-bot')
const { SavePerson } = require('../lib/helpers')

test('person', async t => {
  const { ssb, apollo } = await TestBot()

  async function getPerson (someId) {
    const res = await apollo.query({
      query: `query($someId: String!) {
        person(id: $someId) {
          id
          preferredName
        }
      }`,
      variables: { someId }
    })

    if (res.errors) {
      console.error(JSON.stringify(res.errors, null, 2))
      throw res.errors
    }
    return res.data.person
  }

  // create a publicPerson
  const feedId = ssb.id
  const profileId = await p(ssb.profile.person.public.create)({
    preferredName: 'ziva',
    authors: {
      add: [feedId]
    }
  })

  // link profile to a feedId
  await p(ssb.profile.link.create)({ type: 'feed-profile', profile: profileId })

  // set up listener to see how many times db is hit
  let callCount = 0
  ssb.profile.person.public.get.hook((fn, args) => {
    if (args[0] === profileId) callCount++
    fn(...args)
  })

  /* get by profileId */
  let profileByProfileId = await getPerson(profileId)
  t.equal(callCount, 1, 'initial profile.get call')

  t.true(isMsg(profileByProfileId.id), 'profile.id')
  t.equal(profileByProfileId.preferredName, 'ziva', 'profile.preferredName')

  await getPerson(profileId)
  t.equal(callCount, 1, 'cache working (profile.get not called)')

  await p(ssb.profile.person.public.update)(profileId, { gender: 'female' })
  profileByProfileId = await getPerson(profileId)
  t.equal(callCount, 2, 'cache was cleared after update (profile.get called)')

  /* get by feedId */
  const profileByFeedId = await getPerson(feedId)
  t.deepEqual(profileByFeedId, profileByProfileId, 'get profile by feedId')
  t.equal(callCount, 2, 'cache was used')

  ssb.close()
  t.end()
})

async function adminProfileSetup (opts = {}) {
  const {
    owned = true,
    adminProfile = true
  } = opts

  const admin = await TestBot()
  const { ssb } = admin

  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  // ////
  // create them a profile in the group
  // ////
  const groupProfileId = await p(ssb.profile.person.group.create)({
    preferredName: 'Alice',
    authors: {
      add: [ssb.id]
    },
    recps: [groupId]
  })
  if (owned) await p(ssb.profile.link.create)(groupProfileId)

  let adminProfileId
  if (adminProfile) {
    // ////
    // create them a profile in the admin subgroup
    // ////
    adminProfileId = await p(ssb.profile.person.admin.create)({
      preferredName: 'Alice',
      phone: '021 1234 567', // private contact details that are allowed on admin profiles but not group profiles
      authors: {
        add: [ssb.id]
      },
      recps: [poBoxId, ssb.id]
    })
    if (owned) await p(ssb.profile.link.create)(adminProfileId)
    else await p(ssb.profile.link.create)(groupProfileId, { profileId: adminProfileId })
  }

  return { admin, groupId, poBoxId, groupProfileId, adminProfileId }
}

test('person.adminProfile (can see)', async t => {
  const { admin, groupId, poBoxId, groupProfileId, adminProfileId } = await adminProfileSetup()

  // ////
  // query the person profile
  // ////

  const res = await admin.apollo.query({
    query: `query {
      person(id: "${groupProfileId}") {
        id
        preferredName
        phone
        recps
        adminProfile {
          id
          preferredName
          phone
          recps
        }
      }
    }`
  })

  t.error(res.errors, 'query person')

  const person = res.data.person

  t.deepEqual(
    person,
    {
      id: groupProfileId,
      preferredName: 'Alice',
      phone: null, // the group profile does not contain the private information
      recps: [groupId],
      adminProfile: {
        id: adminProfileId,
        preferredName: 'Alice',
        phone: '021 1234 567', // the admin profile contains the private information
        recps: [poBoxId, admin.ssb.id]
      }
    },
    'can see the correct nested admin profile'
  )

  admin.ssb.close()
  t.end()
})

test('person.adminProfile (can not see)', async t => {
  const { admin, groupId, groupProfileId } = await adminProfileSetup()

  const member = await TestBot()
  await p(admin.ssb.tribes.invite)(groupId, [member.ssb.id], {})

  await replicate({ from: admin.ssb, to: member.ssb, log: false })

  await new Promise(resolve => setTimeout(resolve, 400))

  // ////
  // query the person profile
  // ////

  const getProfile = async () => {
    const res = await member.apollo.query({
      query: `query {
        person(id: "${groupProfileId}") {
          id
          preferredName
          phone
          recps
          adminProfile {
            id
            preferredName
            phone
            recps
          }
        }
      }`
    })
    t.error(res.errors, 'query person')

    return res.data.person
  }

  const profile = await getProfile()

  t.deepEqual(
    profile,
    {
      id: groupProfileId,
      preferredName: 'Alice',
      phone: null, // the group profile does not contain the private information
      recps: [groupId],
      adminProfile: null
    },
    'can not see the admin profile'
  )

  member.ssb.close()
  admin.ssb.close()
  t.end()
})

test('adminProfile (unowned)', async t => {
  const { admin, groupId, groupProfileId } = await adminProfileSetup({ owned: false, adminProfile: false })
  let res

  const savePerson = SavePerson(admin.apollo)

  res = await admin.apollo.query({
    query: `query {
      person(id: "${groupProfileId}") {
        adminProfile {
          phone
        }
      }
    }`
  })
  t.equal(res.data.person.adminProfile, null, 'admin profile does not exist yet')

  // stimulate an adminProfile to be created and linked
  res = await savePerson({
    id: groupProfileId,
    phone: '12345'
  })

  res = await admin.apollo.query({
    query: `query {
      person(id: "${groupProfileId}") {
        adminProfile {
          phone
        }
      }
    }`
  })

  // we expect to see adminProfile here
  t.equal(res.data.person?.adminProfile?.phone, '12345', 'can access the adminProfile details')

  /* see if group member can access adminProfile */
  const member = await TestBot()
  await p(admin.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await replicate({ from: admin.ssb, to: member.ssb, log: false })

  await new Promise(resolve => setTimeout(resolve, 400))

  res = await member.apollo.query({
    query: `query {
      person(id: "${groupProfileId}") {
        adminProfile {
          id
          phone
        }
      }
    }`
  })
  // we expect to see adminProfile here
  console.log('^ harmless error (from ssb-crut failing to load the admin profile')
  t.equal(res.data.person?.adminProfile, null, 'general members cant access the adminProfile details')

  admin.ssb.close()
  member.ssb.close()

  t.end()
})

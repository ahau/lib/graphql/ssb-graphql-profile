const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')

test('list-person', async t => {
  const { ssb, apollo } = await TestBot()

  const { groupId, poBoxId } = await p(ssb.tribes.create)({ addPOBox: true })
  const { person, link } = ssb.profile

  const authors = { add: [ssb.id] }
  const [pubA, A, B, adminA] = await Promise.all([ // eslint-disable-line
    person.public.create({ preferredName: 'A public', authors }),
    person.group.create({ preferredName: 'A', authors, recps: [groupId] }),
    person.group.create({ preferredName: 'B', authors, recps: [groupId] }),
    person.admin.create({ preferredName: 'A admin', authors, recps: [poBoxId, ssb.id] })
  ])

  await p(link.create)(A, { profileId: adminA })

  const listPerson = async (type, groupId) => {
    const result = await apollo.query({
      query: `
        query($type: String!, $groupId: String) {
          listPerson(type: $type, groupId: $groupId) {
            type
            preferredName
            recps
            adminProfile {
              preferredName
            }
          }
        }
      `,
      variables: {
        type,
        groupId
      }
    })

    return result.data.listPerson
  }

  t.deepEqual(
    await listPerson('public'),
    [
      {
        type: 'person',
        preferredName: 'A public',
        recps: null,
        adminProfile: null
      }
    ],
    'type = public'
  )

  t.deepEqual(
    await listPerson('group', groupId),
    // .catch(err => { t.error(err); console.log(JSON.stringify(err, null, 2)) })
    [
      {
        type: 'person',
        preferredName: 'B',
        recps: [groupId],
        adminProfile: null
      },
      {
        type: 'person',
        preferredName: 'A',
        recps: [groupId],
        adminProfile: {
          preferredName: 'A admin'
        }
      }
    ],
    'type = group'
  )

  t.deepEqual(
    await listPerson('admin', poBoxId),
    [
      {
        type: 'person/admin',
        preferredName: 'A admin',
        adminProfile: null,
        recps: [poBoxId, ssb.id]
      }
    ],
    'type = admin'
  )

  ssb.close()
  t.end()
})

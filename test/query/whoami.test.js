const tape = require('tape')
const { isFeed, isMsg, isCloakedMsg: isGroup } = require('ssb-ref')
const TestBot = require('../test-bot')

tape('whoami', async t => {
  t.plan(9)
  const { ssb, apollo } = await TestBot({ loadContext: true })

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId
          profileId
          profile { id, recps }
        }
        personal {
          groupId
          profileId
          profile { id recps }
        }
      }
    }`
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const { public: _public, personal } = result.data.whoami

  t.true(isFeed(_public.feedId))
  t.true(isMsg(_public.profileId))
  t.equal(_public.profileId, _public.profile.id)
  t.deepEqual(_public.profile.recps, null, 'public profile recps')

  t.true(isGroup(personal.groupId))
  t.true(isMsg(personal.profileId))
  t.equal(personal.profileId, personal.profile.id)
  t.deepEqual(
    personal.profile.recps,
    [personal.groupId],
    'personal profile recps'
  )

  ssb.close()
})

tape('whoami (pataka)', async t => {
  t.plan(5)
  const { ssb, apollo } = await TestBot({ loadContext: true, isPataka: true })

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          feedId
          profileId
          profile { id, recps }
        }
      }
    }`
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const { public: _public } = result.data.whoami

  t.true(isFeed(_public.feedId))
  t.true(isMsg(_public.profileId))
  t.equal(_public.profileId, _public.profile.id)
  t.deepEqual(_public.profile.recps, null, 'public profile recps')

  ssb.close()
})

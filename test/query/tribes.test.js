const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')

test('tribes', async t => {
  const { ssb, apollo } = await TestBot()

  const fakePoBoxId = 'ssb:identity/po-box/LglSx9dxQVuD8xbrNZ1NSCjpAVOTfFlrRZb0LPR4jH0='

  const tribes = () => {
    return apollo.query({
      query: `{
        tribes {
          id
          public {
            id
            preferredName
            poBoxId
            recps
          }
          private {
            id
            preferredName
            poBoxId
            recps
          }
          subtribes {
            id
            public {
              id
              preferredName
              poBoxId
              recps
            }
            private {
              id
              preferredName
              poBoxId
              recps
            }
          }
        }
      }`
    })
  }

  const subtribes = (id) => {
    return apollo.query({
      query: `
        query($id: String!) {
          subtribes(id: $id) {
            id
            public {
              id
              preferredName
              poBoxId
              recps
            }
            private {
              id
              preferredName
              poBoxId
              recps
            }
          }
        }
      `,
      variables: {
        id
      }
    })
  }

  // input = { id, group, profile, allowPublic, parentGroupId }
  async function saveLink (input) {
    const res = await apollo.mutate({
      mutation: `mutation($input: GroupProfileLinkInput!) {
        saveGroupProfileLink(input: $input)
      }`,
      variables: {
        input
      }
    })

    t.error(res.errors, 'save group-profile link without error')

    return res.data.saveGroupProfileLink
  }

  const { groupId } = await p(ssb.tribes.create)({})

  const profilePublicId = await p(ssb.profile.community.public.create)({
    preferredName: 'public profile',
    poBoxId: fakePoBoxId,
    authors: {
      add: [ssb.id]
    }
  })
  await saveLink({ profile: profilePublicId, group: groupId })

  const profilePrivateId = await p(ssb.profile.privateCommunity.create)({
    preferredName: 'private profile',
    poBoxId: fakePoBoxId,
    recps: [groupId],
    authors: {
      add: [ssb.id]
    }
  })
  await saveLink({ profile: profilePrivateId, group: groupId })

  // create a subgroup + profiles for it
  const { groupId: subgroupId } = await p(ssb.tribes.subtribe.create)(groupId, null)

  const subgroupPublicProfileId = await p(ssb.profile.community.public.create)({
    preferredName: 'public sub profile',
    poBoxId: fakePoBoxId,
    authors: {
      add: [ssb.id]
    }
  })

  await saveLink({ profile: subgroupPublicProfileId, group: subgroupId, parentGroupId: groupId })

  const subgroupPrivateProfileId = await p(ssb.profile.privateCommunity.create)({
    preferredName: 'private sub profile',
    poBoxId: fakePoBoxId,
    authors: {
      add: [ssb.id]
    },
    recps: [subgroupId]
  })

  await saveLink({ profile: subgroupPrivateProfileId, group: subgroupId, parentGroupId: groupId })

  let getCount = 0
  ssb.profile.community.public.get.hook((fn, args) => {
    getCount++
    fn(...args)
  })
  ssb.profile.community.group.get.hook((fn, args) => {
    getCount++
    fn(...args)
  })

  const expectedSubtribes = [
    {
      id: subgroupId,
      public: [{
        id: subgroupPublicProfileId,
        preferredName: 'public sub profile',
        poBoxId: fakePoBoxId,
        recps: null
      }],
      private: [{
        id: subgroupPrivateProfileId,
        preferredName: 'private sub profile',
        poBoxId: fakePoBoxId,
        recps: [subgroupId]
      }]
    }
  ]

  const expected = [
    {
      id: groupId,
      public: [{
        id: profilePublicId,
        preferredName: 'public profile',
        poBoxId: fakePoBoxId,
        recps: null
      }],
      private: [{
        id: profilePrivateId,
        preferredName: 'private profile',
        poBoxId: fakePoBoxId,
        recps: [groupId]
      }],
      subtribes: expectedSubtribes
    }
  ]

  let res = await tribes()

  t.error(res.errors, 'gets tribes without error')
  t.deepEqual(
    res.data.tribes,
    expected,
    'gets tribes data'
  )

  t.equal(getCount, 4, 'profile cache not used for initial query')

  await tribes()
  t.equal(getCount, 4, 'profile-cache used for subsequent query')

  res = await subtribes(groupId)
  t.error(res.errors, 'gets subtribes without error')

  t.deepEqual(
    res.data.subtribes,
    expectedSubtribes,
    'returns the same subtribes as the tribes query'
  )

  ssb.close()
  t.end()
})

test('tribes (excludes tombstoned tribes)', async t => {
  const { ssb, apollo } = await TestBot()

  const { groupId } = await p(ssb.tribes.create)({})
  const communityProfileId = await p(ssb.profile.community.group.create)({
    authors: { add: ['*'] },
    recps: [groupId]
  })

  await p(ssb.profile.link.create)(communityProfileId, { groupId })

  let res = await apollo.query({
    query: `query {
      tribes {
        id
        private { id }
      }
    }`
  })

  t.deepEqual(
    res.data.tribes,
    [
      {
        id: groupId,
        private: [{ id: communityProfileId }]
      }
    ],
    'tribe initially present'
  )

  await p(ssb.profile.community.group.tombstone)(communityProfileId, {})

  res = await apollo.query({
    query: `query {
      tribes {
        id
        private { id }
      }
    } `
  })

  t.deepEqual(
    res.data.tribes,
    [],
    'tribe with tombstoned private profile is not listed'
  )

  ssb.close()
  t.end()
})

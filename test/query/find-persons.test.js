const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../test-bot')

function FindPersons (t, apollo) {
  return async function findPersons (name) {
    const res = await apollo.query({
      query: `
        query($name: String!) {
          findPersons(name: $name) {
            id
            preferredName
            legalName
            gender
            source
            aliveInterval
            birthOrder
            description
            altNames
            avatarImage { uri }
            recps
          }
        }
      `,
      variables: { name }
    })

    t.error(res.errors, 'find persons without error')

    return res.data.findPersons
  }
}

test('findPersons', async t => {
  t.plan(3)
  const { ssb, apollo } = await TestBot()
  const { groupId } = await p(ssb.tribes.create)({})

  const findPersons = FindPersons(t, apollo)

  // save a bunch of profiles
  const details = (name) => ({
    preferredName: name,
    authors: {
      add: [ssb.id]
    },
    recps: [groupId]
  })
  const create = (name) => p(ssb.profile.person.group.create)(details(name))

  const profileAId = await create('Cherese')
  const profileBId = await create('Cherry')
  const profileCId = await create('Chester')

  const profileSearch = await findPersons('che')

  t.true(profileSearch.length === 3, 'find person returns 3 suggestions')
  const [A, B, C] = profileSearch

  t.deepEquals([A.id, B.id, C.id].sort(), [profileCId, profileBId, profileAId].sort(), 'suggestions match')

  ssb.close()
})

test('find persons (duplicate profiles)', async t => {
  t.plan(3)
  const { ssb, apollo } = await TestBot()
  const { groupId } = await p(ssb.tribes.create)({})

  const findPersons = FindPersons(t, apollo)

  // save a bunch of profiles
  const details = (name) => ({
    preferredName: name,
    authors: {
      add: [ssb.id]
    },
    recps: [groupId]
  })
  const create = (name) => p(ssb.profile.person.group.create)(details(name))
  const linkFeed = (profileId) => p(ssb.profile.link.create)({ profile: profileId })

  // create and link personal profile
  const personalProfileId = await create('Cherese')
  await linkFeed(personalProfileId)

  // // create an link a duplicate personal profile
  const duplicatePersonalProfileId = await create('Cherese')
  await linkFeed(duplicatePersonalProfileId)

  const otherProfileId1 = await create('Chester')
  const otherProfileId2 = await create('Cherry')

  const profileSearch = await findPersons('che')

  t.true(profileSearch.length === 3, 'find person returns 3 suggestions')
  const [A, B, C] = profileSearch

  t.deepEquals([A.id, B.id, C.id].sort(), [otherProfileId1, otherProfileId2, personalProfileId].sort(), 'suggestions match')

  ssb.close()
})

test('find persons (filter by: type, groupId)', async t => {
  const { ssb, apollo } = await TestBot()
  const { groupId } = await p(ssb.tribes.create)({})

  // save a bunch of profiles
  const details = (name, groupId) => ({
    preferredName: name,
    authors: {
      add: [ssb.id]
    },
    recps: groupId ? [groupId] : undefined
  })

  // create and link source profile
  const sourceProfileId = await p(ssb.profile.person.source.create)(details('Cherese', groupId))
  const groupProfileId = await p(ssb.profile.person.group.create)(details('Cherese', groupId))
  const publicProfileId = await p(ssb.profile.person.public.create)(details('Cherese'))

  // filter by type: 'person/source'
  let res = await apollo.query({
    query: `
      query($name: String!, $type: String) {
        findPersons(name: $name, type: $type) {
          id
        }
      }
    `,
    variables: {
      name: 'che',
      type: 'person/source'
    }
  })
  t.deepEquals(res.data.findPersons, [{ id: sourceProfileId }], 'source profile found')

  // filter by type null
  res = await apollo.query({
    query: `
      query($name: String!, $type: String) {
        findPersons(name: $name, type: $type) {
          id
        }
      }
    `,
    variables: {
      name: 'che',
      type: null
    }
  })
  t.deepEquals(
    res.data.findPersons,
    [{ id: sourceProfileId }, { id: groupProfileId }, { id: publicProfileId }],
    'all profiles found'
  )

  // filter by groupId
  res = await apollo.query({
    query: `
      query($name: String!, $groupId: String) {
        findPersons(name: $name, groupId: $groupId) {
          id
        }
      }
    `,
    variables: {
      name: 'che',
      groupId
    }
  })
  t.deepEquals(res.data.findPersons, [{ id: groupProfileId }], 'groupId profile found (default type: person)')

  // filter by groupId , type: null
  res = await apollo.query({
    query: `
      query($name: String!, $type: String, $groupId: String) {
        findPersons(name: $name, type: $type, groupId: $groupId) {
          id
        }
      }
    `,
    variables: {
      name: 'che',
      type: null,
      groupId
    }
  })
  t.deepEquals(res.data.findPersons, [{ id: sourceProfileId }, { id: groupProfileId }], 'groupId profile found')

  ssb.close()
  t.end()
})

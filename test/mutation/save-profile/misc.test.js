const test = require('tape')
const { isMsgId } = require('ssb-ref')
const { promisify: p } = require('util')

const TestBot = require('../../test-bot')
const { getProfile, SavePerson, GetPerson } = require('../../lib/helpers')

const MB = 1024 * 1024

test('savePerson (create person.public)', async (t) => {
  t.plan(1)
  const { ssb, apollo } = await TestBot()

  const savePerson = SavePerson(apollo)

  const details = {
    type: 'person',
    authors: {
      add: [ssb.id]
    }
  }

  const result = await savePerson(details)

  t.true(
    isMsgId(result.data.savePerson),
    'saves a minimal profile, returning profileId'
  )

  ssb.close()
})

test('savePerson (update person.source)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot()
  const { groupId } = await p(ssb.tribes.create)({})

  const savePerson = SavePerson(apollo)
  const getPerson = GetPerson(apollo)

  // ///////
  // Create and link initial profile
  // ///////

  const initial = await savePerson({
    type: 'person/source',

    preferredName: 'Cherese',
    altNames: { add: ['cheche', 'reese'] },
    legalName: 'Cherese Eriepa',
    description: 'Developer working on Ahau',
    gender: 'female',
    aliveInterval: '1995-07-XX/*',
    birthOrder: 2,
    deceased: false,
    placeOfBirth: 'Hamilton, New Zealand',
    placeOfDeath: 'N/A',
    buriedLocation: 'N/A',

    avatarImage: {
      blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
      unbox: '4bOkwCLbw5B9d5+H59oxDNOyIaOhfLfqOLm1MGKyTLI=.boxs',
      mimeType: 'image/png',
      size: 4 * MB,
      width: 500,
      height: 480
    },
    headerImage: {
      blob: '&CLbw5B9d5+H59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI=.sha256',
      mimeType: 'image/png'
    },

    address: '1234 Example Street',
    city: 'Cambridge',
    country: 'New Zealand',
    postCode: '3434',
    phone: '02712345678',
    email: 'cherese@email.com',
    profession: 'Software Engineer',

    education: ['Bachelor of Science'],
    school: ['The University of Waikato'],

    authors: {
      add: [ssb.id]
    },

    recps: [groupId]
  })

  t.error(initial.errors, 'throws no errors when saving the person')

  const profileId = initial.data.savePerson
  t.true(
    isMsgId(profileId),
    'saves a fully featured profile/person/source, returning profileId'
  )

  // link the profile to the feedId
  await p(ssb.profile.link.create)(profileId)

  // ///////
  // Get initial profile
  // ///////

  const initialGet = await getPerson(profileId)

  t.deepEqual(
    initialGet.data.person,
    {
      id: profileId,
      type: 'person/source',
      recps: [groupId],

      preferredName: 'Cherese',
      legalName: 'Cherese Eriepa',
      altNames: ['cheche', 'reese'],
      description: 'Developer working on Ahau',

      avatarImage: {
        uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256?unbox=4bOkwCLbw5B9d5%2BH59oxDNOyIaOhfLfqOLm1MGKyTLI%3D.boxs'
      },
      headerImage: {
        uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256'
      },

      phone: '02712345678',
      email: 'cherese@email.com',

      address: '1234 Example Street',
      city: 'Cambridge',
      country: 'New Zealand',
      postCode: '3434',
      profession: 'Software Engineer',

      gender: 'female',
      source: null,
      aliveInterval: '1995-07-XX/*',
      birthOrder: 2,
      deceased: false,
      placeOfBirth: 'Hamilton, New Zealand',
      placeOfDeath: 'N/A',
      buriedLocation: 'N/A',

      education: ['Bachelor of Science'],
      school: ['The University of Waikato'],
      customFields: []
    },
    'can retrieve initial profile using the person query'
  )

  // ///////
  // Update initial profile
  // ///////

  const update = await savePerson({
    id: profileId, // << this makes it an update

    legalName: 'Cherese Putiputi Eriepa',
    altNames: { remove: ['reese'] },
    aliveInterval: '199X-07-XX/*'
  })
  t.error(update.errors, 'updates profile without error')

  // ///////
  // Get updated profile using the two options
  // ///////

  // get person
  const updatedGet = await getPerson(profileId)

  const expectedPerson = {
    id: profileId,
    type: 'person/source',
    recps: [groupId],

    preferredName: 'Cherese',
    legalName: 'Cherese Putiputi Eriepa',
    altNames: ['cheche'],
    description: 'Developer working on Ahau',

    avatarImage: { uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256?unbox=4bOkwCLbw5B9d5%2BH59oxDNOyIaOhfLfqOLm1MGKyTLI%3D.boxs' },
    headerImage: { uri: 'http://localhost:26835/get/%26CLbw5B9d5%2BH59oxDNOy4bOkwIaOhfLfqOLm1MGKyTLI%3D.sha256' },

    phone: '02712345678',
    email: 'cherese@email.com',

    address: '1234 Example Street',
    city: 'Cambridge',
    country: 'New Zealand',
    postCode: '3434',
    profession: 'Software Engineer',

    gender: 'female',
    source: null,
    aliveInterval: '199X-07-XX/*',
    birthOrder: 2,
    deceased: false,
    placeOfBirth: 'Hamilton, New Zealand',
    placeOfDeath: 'N/A',
    buriedLocation: 'N/A',

    education: ['Bachelor of Science'],
    school: ['The University of Waikato'],
    customFields: []
  }

  t.deepEqual(
    updatedGet.data.person,
    expectedPerson,
    'can retrieve updated profile using the person query'
  )

  // get profile
  const profileRes = await apollo.query(
    getProfile(profileId)
  )
  t.error(profileRes.errors, 'get profile without error')
  t.deepEqual(
    profileRes.data.profile,
    expectedPerson,
    'can retrieve updated profile using the profile query'
  )

  ssb.close()
})

test('savePerson (with ssb-recps-guard)', async (t) => {
  t.plan(4)
  const { ssb, apollo } = await TestBot({ recpsGuard: true })

  const savePerson = SavePerson(apollo)

  const authors = {
    add: [ssb.id],
    remove: []
  }

  const details = {
    type: 'person',
    authors
  }

  const result = await savePerson(details)

  t.match(result.errors[0].message, /Error: recps-guard: public messages of type "profile\/person" not allowed/, 'recps-guard blocks publish')

  const result2 = await savePerson({ type: 'person', preferredName: 'mix', allowPublic: true, authors })

  t.equal(result2.errors, undefined, 'saves public profile')
  const id = result2.data.savePerson

  /* update */
  const result3 = await savePerson({ id, preferredName: 'mixmix', authors })

  t.match(result3.errors[0].message, /Error: recps-guard: public messages of type "profile\/person" not allowed/, 'recps-guard blocks publish')
  const result4 = await savePerson({ id, preferredName: 'mixmix', allowPublic: true, authors })
  t.equal(result4.errors, undefined, 'updates public profile')

  ssb.close()
})

test('savePerson (tombstone public profile only)', async (t) => {
  t.plan(7)
  const { ssb, apollo } = await TestBot({ loadContext: true })

  const savePerson = SavePerson(apollo)

  const result = await apollo.query({
    query: `{
      whoami {
        public {
          profileId
          profile { id, recps }
        }
        personal {
          groupId
          profileId
          profile { id recps }
        }
      }
    }`
  })

  // Ensure that basic query worked.
  t.error(result.errors, 'query should not return errors')

  const whoami = result.data.whoami

  const tombstone = {
    date: '2023-03-21T22:31:16.480Z',
    reason: 'User deleting profile'
  }

  const updatePublic = await savePerson({
    id: whoami.public.profileId, // << this makes it an update
    allowPublic: true,
    tombstone // tombstone public profile
  })
  t.error(updatePublic.errors, 'Tombstone public profile without error')

  const updatePersonal = await savePerson({
    id: whoami.personal.profileId, // << this makes it an update
    tombstone
  })

  t.error(updatePersonal.errors, 'doesnt throw an error when tombstoning your personal profile')
  // t.true(updatePersonal.errors, 'Throws error when tombstone personal profile')

  const tombstoneRes = await apollo.query({
    query: `{
      whoami {
        public {
          profile { 
            id
            recps
            tombstone {  
              date
              reason
            } 
          }
        }
        personal {
          profile { 
            id
            recps
            tombstone {  
              date
              reason
            } 
          }
        }
      }
    }`
  })

  t.error(tombstoneRes.errors, 'gets whoami without error')
  const whoamiUpdated = tombstoneRes.data.whoami

  t.deepEqual(
    whoamiUpdated,
    {
      public: {
        profile: {
          id: whoami.public.profile.id,
          recps: null,
          tombstone: {
            date: whoamiUpdated.public.profile.tombstone.date, // hack: cant determine the date
            reason: tombstone.reason
          }
        }
      },
      personal: {
        profile: {
          id: whoamiUpdated.personal.profile.id,
          recps: [whoami.personal.groupId],
          tombstone: {
            date: whoamiUpdated.personal.profile.tombstone.date, // hack: cant determine the date
            reason: tombstone.reason
          }
        }
      }
    },
    'Can query personal and public profiles after tombstoning public profile'
  )

  const getPerson = GetPerson(apollo)

  // see what happens when we try fetch the profile by feedId
  const res = await getPerson(ssb.id)

  t.error(res.errors, 'doesnt return errors when getting profile by feedId')

  t.equal(res.data.person, null, 'when cannot getProfile, graphql returns null')

  ssb.close()
})

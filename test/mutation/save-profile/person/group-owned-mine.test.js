const test = require('tape')
const { promisify: p } = require('util')
const LegacyCrut = require('ssb-profile/legacy')

const TestBot = require('../../../test-bot')
const { SavePerson } = require('../../../lib/helpers')

test('savePerson (your profile)', async (t) => {
  t.plan(14)
  const { ssb, apollo } = await TestBot({ loadContext: true, recpsGuard: true })

  const saveCurrentIdentity = SavePerson(apollo)

  const legacyCrut = LegacyCrut(ssb)

  // setup functions for creating things...
  const linkToFeedId = p(ssb.profile.link.create)
  const get = p(legacyCrut.get)

  async function getPreferredName (profileId) {
    const res = await get(profileId)
      .catch(err => {
        console.error('ssb-profile/legacy failed', err)
        return { errors: err }
      })
    if (res.errors) return 'FAILED TO GET preferredName'
    return res.preferredName
  }

  // create profile.person.group
  const { groupId: eriepaTribeId } = await p(ssb.tribes.create)({})
  const profileInEriepaTribe = await p(ssb.profile.person.group.create)({
    preferredName: 'Cherese - Eriepa Tribe',
    authors: { add: [ssb.id] },
    recps: [eriepaTribeId]
  })

  await linkToFeedId(profileInEriepaTribe)

  // create profile.person.admin
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(eriepaTribeId, { addPOBox: true, admin: true })
  const profileInDeisherTribe = await p(ssb.profile.person.admin.create)({
    preferredName: 'Cherese - Deisher Tribe',
    authors: { add: [ssb.id] },
    recps: [poBoxId, ssb.id]
  })
  await linkToFeedId(profileInDeisherTribe)

  // validate the preferredNames of each profile
  t.deepEqual(await getPreferredName(profileInEriepaTribe), 'Cherese - Eriepa Tribe', 'eriepa tribe profile matches original')
  t.deepEqual(await getPreferredName(profileInDeisherTribe), 'Cherese - Deisher Tribe', 'deisher tribe profile matches original')

  // // update one of the profiles
  const res = await saveCurrentIdentity({
    id: profileInEriepaTribe,
    preferredName: 'Cherese',
    altNames: {},
    phone: '0221234987'
  })
  t.false(res.errors, 'savePerson returns no errors')

  const profileIds = []
  // // loop through all linked profiles and check if they are all 'Cherese'
  ssb.profile.findByFeedId(ssb.id, async (err, profiles) => {
    if (err) throw err

    profiles = [...profiles.private, ...profiles.public]

    profiles.forEach(profile => {
      profileIds.push(profile.key)
      const { preferredName, phone } = profile

      if (profile.type === 'person') {
        const subType = profile.recps ? 'group' : 'public'

        t.deepEqual(preferredName, 'Cherese', `${profile.type} (${subType}) preferredName`)
        t.deepEqual(phone, undefined, `${profile.type} (${subType}) phone NOT added`)
      }

      if (profile.type === 'person/source' || profile.type === 'person/admin') {
        t.deepEqual(preferredName, 'Cherese', profile.type + ' preferredName')
        t.deepEqual(phone, '0221234987', profile.type + ' phone')
      }
    })

    t.deepEqual(res.data.savePerson, profileInEriepaTribe, 'returns the id')

    // // attempt to update with a tombstone
    const tombstoneRes = await saveCurrentIdentity({ id: profileInEriepaTribe, tombstone: { reason: 'delete' } })
    t.error(tombstoneRes.errors, 'doesnt fail on tombstone')

    const profile = await p(ssb.profile.person.group.get)(profileInEriepaTribe)

    t.deepEqual(profile.tombstone?.reason, 'delete', 'profile in group was tombstoned')

    ssb.close()
  })
})

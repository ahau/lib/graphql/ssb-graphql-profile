const test = require('tape')
const { promisify: p } = require('util')
const pull = require('pull-stream')
const { live, toPullStream, where, author, and, isDecrypted } = require('ssb-db2/operators')

const TestBot = require('../../../test-bot')
const { SavePerson } = require('../../../lib/helpers')

function Save (apollo, t) {
  const savePerson = SavePerson(apollo)

  return async function save (input) {
    const res = await savePerson(input)

    if (res.errors) {
      t.error(res.errors, 'saves profile without error')
      return
    }

    return res.data.savePerson
  }
}

/*
Steps to reproduce in the front end:
1. Create a new profile in a whakapapa
2. Go to the persons list, and edit that profile by adding an email address
      An admin profile was created with the email, and linked to the group profile
      The email address showed in the list on the profile
3. Edit the same profile again, with a different email
      The email update didnt show in the list
      Looking at the backend console, it looks like a new admin profile was created with the email and linked to that same group profile <—- BUG
*/
test('savePerson (admin profile bug)', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: true, recpsGuard: true })
  const savePerson = Save(apollo, t)
  const findAdminProfileLinks = p(ssb.profile.person.group.findAdminProfileLinks)

  // fail if there's an empty update published
  const isEmptyUpdate = msg => Object.keys(msg.value.content).length === 3
  // "empty" update content { type, tangles, recps }
  pull(
    ssb.db.query(
      where(
        and(
          author(ssb.id),
          isDecrypted('box2')
        )
      ),
      live({ old: false }),
      toPullStream()
    ),
    pull.filter(isEmptyUpdate),
    pull.drain(m => {
      console.log(m.value.sequence, m.key, JSON.stringify(m.value.content, null, 2))
      t.fail('empty update was published')
    })
  )

  // create a group with an admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: adminGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true }) // eslint-disable-line

  // make a group profile
  const groupProfileId = await savePerson({
    type: 'person/profile',
    preferredName: 'Cherese',

    // required details
    authors: {
      add: ['*']
    },
    recps: [groupId]
  })

  // update the profile with a phone (admin only detail)
  await savePerson({
    id: groupProfileId,
    phone: '021 1234 567'
  })

  const initialLinks = await findAdminProfileLinks(groupProfileId, {})
  t.true(initialLinks.parentLinks.length === 1, 'returned one link')

  const { child, parent: adminProfileId } = initialLinks.parentLinks[0]
  t.equal(child, groupProfileId, 'links group profile + admin group profile')

  // save another update to the profile
  await savePerson({
    id: groupProfileId,
    phone: '021 1111 222'
  })

  const updatedLinks = await findAdminProfileLinks(groupProfileId, {})
  t.true(updatedLinks.parentLinks.length === 1, 'returned one link')
  t.equals(updatedLinks.parentLinks[0].key, initialLinks.parentLinks[0].key, 'the one link has the same key')

  const adminProfile = await p(ssb.profile.person.admin.get)(adminProfileId)
  t.equal(adminProfile.phone, '021 1111 222', 'admin profile was updated')

  ssb.close()
  t.end()
})

const test = require('tape')
const pull = require('pull-stream')
const { promisify: p } = require('util')
const { where, author, live, toPullStream } = require('ssb-db2/operators')

const { SavePerson } = require('../../../lib/helpers')
const TestBot = require('../../../test-bot')

/*
cases to test:
- you own it > bulk update
- someone else owns it
  - if you're admin, create/ update the admin-profile
- no-one owns it (unowned)
  - create/ update group profile
  - if you're admin, create/ update the admin-profile

*/

function SaveProfile (apollo, t) {
  const savePerson = SavePerson(apollo)
  return async function savePersonGroup (input) {
    const res = await savePerson(input)

    t.error(res.errors, input.id ? 'savePerson returned no errors for update' : 'savePerson return no errors for create')

    return res.data.savePerson
  }
}

// this checks what happens when an admin creates or updates an unowned group profile
// in both cases, we expect any admin-only details to be save to an admin-group profile
//
// TODO we also need to modify the profile query to find links to admin-profiles

test('savePerson - person.group.create (unowned, adminDetails)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: subGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  // ////
  // Create + Test saving profiles works for person (group) profiles
  // ////
  const input = {
    type: 'person',
    preferredName: 'Alice',
    authors: { add: ['*'] },

    phone: '2717777777777', // << for admins only!
    recps: [groupId]
  }
  const publishedMsgs = []

  const stream = new Promise(resolve => {
    pull(
      ssb.db.query(
        where(author(ssb.id)),
        live({ old: false }),
        toPullStream()
      ),
      pull.drain(m => {
        publishedMsgs.push(m)

        if (publishedMsgs.length < 3) return

        // ////
        // check it created the unowned person profile in the group
        // ////
        const groupProfileId = publishedMsgs[0].key
        const groupProfile = publishedMsgs[0].value.content
        t.deepEqual(
          {
            type: groupProfile.type,
            preferredName: groupProfile.preferredName?.set,
            phone: groupProfile.phone?.set,
            recps: groupProfile.recps
          },
          {
            type: 'profile/person',
            preferredName: input.preferredName,
            phone: undefined,
            recps: [groupId]
          },
          'the group profile was created'
        )

        // ////
        // check it created the unowned person profile in the admin group
        // ////
        const adminProfileId = publishedMsgs[1].key
        const adminProfile = publishedMsgs[1].value.content
        t.deepEqual(
          {
            type: adminProfile.type,
            preferredName: adminProfile.preferredName?.set,
            phone: adminProfile.phone?.set,
            recps: adminProfile.recps
          },
          {
            type: 'profile/person/admin',
            preferredName: undefined,
            phone: input.phone,
            recps: [subGroupId]
          },
          'the admin profile was created'
        )

        // ////
        // check it created a link for the two profiles
        // ////
        const link = publishedMsgs?.[2]?.value?.content || {}
        t.deepEqual(
          {
            type: link.type,
            parent: link.parent,
            child: link.child,
            recps: link.recps
          },
          {
            type: 'link/profile-profile/admin',
            parent: adminProfileId,
            child: groupProfileId,
            recps: [groupId]
          },
          'link for group-admin profiles created'
        )

        resolve()
      })
    )
  })

  // ////
  // create the profile!
  // ////
  await savePersonGroup(input)

  await stream

  await p(ssb.close)(true)

  t.end()
})

test('savePerson - person.group.create (unowned, no adminDetails)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: subGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true }) // eslint-disable-line

  // ////
  // Create + Test saving profiles works for person (group) profiles
  // ////
  const input = {
    type: 'person',
    preferredName: 'Alice',
    authors: { add: ['*'] },
    // NOTE no admin details

    recps: [groupId]
  }
  const publishedMsgs = []

  pull(
    ssb.db.query(
      where(author(ssb.id)),
      live({ old: false }),
      toPullStream()
    ),
    pull.drain(m => {
      publishedMsgs.push(m)

      if (publishedMsgs.length > 1) throw new Error('should not have made an admin profile!')

      // ////
      // check it created the unowned person profile in the group
      // ////
      const groupProfile = publishedMsgs[0].value.content
      t.deepEqual(
        {
          type: groupProfile.type,
          preferredName: groupProfile.preferredName?.set,
          phone: groupProfile.phone?.set,
          recps: groupProfile.recps
        },
        {
          type: 'profile/person',
          preferredName: input.preferredName,
          phone: undefined,
          recps: [groupId]
        },
        'the group profile was created'
      )

      setTimeout(() => {
        ssb.close()
        t.end()
      }, 100)
    })
  )

  // ////
  // create the profile!
  // ////
  await savePersonGroup(input)
})

test('savePerson - person.group.update (unowned, no adminProfile yet)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: subGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  // ////
  // Create + Test saving profiles works for person (group) profiles
  // ////

  // ////
  // create the profile!
  // ////
  const input = {
    type: 'person',
    authors: { add: ['*'] },
    recps: [groupId]
  }

  const groupProfileId = await savePersonGroup(input)

  const publishedMsgs = []

  const stream = new Promise(resolve => {
    pull(
      ssb.db.query(
        where(author(ssb.id)),
        live({ old: false }),
        toPullStream()
      ),
      pull.drain(m => {
        publishedMsgs.push(m)

        if (publishedMsgs.length < 3) return

        // ////
        // check it updated the unowned person profile in the group
        // ////
        const groupProfile = publishedMsgs?.[0]?.value?.content || {}

        t.deepEqual(
          {
            type: groupProfile.type,
            preferredName: groupProfile.preferredName?.set,
            phone: groupProfile.phone?.set,
            recps: groupProfile.recps
          },
          {
            type: 'profile/person',
            preferredName: 'Alice',
            phone: undefined,
            recps: [groupId]
          },
          'the group profile was  updated'
        )

        // // ////
        // // check it updated the unowned person profile in the admin group
        // // ////
        const adminProfileId = publishedMsgs[1].key
        const adminProfile = publishedMsgs[1].value.content

        t.deepEqual(
          {
            type: adminProfile.type,
            preferredName: adminProfile.preferredName?.set,
            phone: adminProfile.phone?.set,
            recps: adminProfile.recps
          },
          {
            type: 'profile/person/admin',
            preferredName: undefined,
            phone: '2717777777777',
            recps: [subGroupId]
          },
          'the admin profile was updated'
        )

        // ////
        // check it created a link for the two profiles
        // ////
        const link = publishedMsgs?.[2]?.value?.content || {}
        t.deepEqual(
          {
            type: link.type,
            parent: link.parent,
            child: link.child,
            recps: link.recps
          },
          {
            type: 'link/profile-profile/admin',
            parent: adminProfileId,
            child: groupProfileId,
            recps: [groupId]
          },
          'link for group-admin profiles created'
        )

        resolve()
      })
    )
  })
  // ////
  // update the profile!
  // ////
  const updateInput = {
    id: groupProfileId,
    preferredName: 'Alice',
    phone: '2717777777777' // << for admins only!
  }

  // TODO: if i update the preferredName for the group profile, does it update the one for the admin profile too? these tests indicate that it does

  await savePersonGroup(updateInput)

  await stream

  await p(ssb.close)(true)
  t.end()
})

test('savePerson - person.group.update (unowned, existing adminProfile)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: subGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  // ////
  // Create + Test saving profiles works for person (group) profiles
  // ////

  const input = {
    type: 'person',
    authors: { add: ['*'] },

    phone: '12345', // NOTE this stimulates an adminProfile to be created and linked
    recps: [groupId]
  }

  const groupProfileId = await savePersonGroup(input)

  const publishedMsgs = []

  pull(
    ssb.db.query(
      where(author(ssb.id)),
      live({ old: false }),
      toPullStream()
    ),
    pull.drain(m => {
      publishedMsgs.push(m)

      if (publishedMsgs.length < 2) return
      if (publishedMsgs.length > 2) throw new Error('too many messages')
      // expect 2 updates, no link to be created as it already exists
      // - group profile: preferredName
      // - admin profile: phone

      // ////
      // check it updated the unowned person profile in the group
      // ////
      const groupProfile = publishedMsgs?.[0]?.value?.content || {}

      t.deepEqual(
        {
          type: groupProfile.type,
          preferredName: groupProfile.preferredName?.set,
          phone: groupProfile.phone?.set,
          isUpdate: typeof groupProfile?.tangles?.profile?.root === 'string',
          recps: groupProfile.recps
        },
        {
          type: 'profile/person',
          preferredName: 'Alice Irving',
          phone: undefined,
          isUpdate: true,
          recps: [groupId]
        },
        'the group profile was  updated'
      )

      // // ////
      // // check it updated the unowned person profile in the admin group
      // // ////
      const adminProfile = publishedMsgs[1].value.content

      t.deepEqual(
        {
          type: adminProfile.type,
          preferredName: adminProfile.preferredName?.set,
          phone: adminProfile.phone?.set,
          isUpdate: typeof groupProfile?.tangles?.profile?.root === 'string',
          recps: adminProfile.recps
        },
        {
          type: 'profile/person/admin',
          preferredName: undefined,
          phone: '2717777777777',
          isUpdate: true,
          recps: [subGroupId]
        },
        'the admin profile was updated'
      )

      setTimeout(() => {
        ssb.close()
        t.end()
      }, 100)
    })
  )
  // ////
  // update the profile!
  // ////
  const updateInput = {
    id: groupProfileId,
    preferredName: 'Alice Irving',
    phone: '2717777777777' // << for admins only!
  }

  // TODO: if i update the preferredName for the group profile, does it update the one for the admin profile too? these tests indicate that it does

  await savePersonGroup(updateInput)
})

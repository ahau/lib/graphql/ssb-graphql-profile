const test = require('tape')
const { promisify: p } = require('util')
const TestBot = require('../../../test-bot')
const { SavePerson, GetPerson, InitGroup, DeletePerson, sleep } = require('../../../lib/helpers')

const getPersonQuery = (id) => ({
  query: `query($id: String!) {
    person(id: $id) {
      id
      recps
      adminProfile {
        id
        email
        tombstone {
          reason
        }
      }
      tombstone {
        reason
      }
    }
  }`,
  variables: {
    id
  }
})

function TestbotHelpers (testbot, t) {
  const savePerson = SavePerson(testbot.apollo)
  const getPerson = GetPerson(testbot.apollo, getPersonQuery)
  const deletePerson = DeletePerson(testbot.apollo)

  return {
    initGroup: InitGroup(testbot.ssb),
    savePerson: async (input, errorCheck = true) => {
      const res = await savePerson(input)
      if (errorCheck) {
        t.error(res.errors, 'saves person without error')
        if (res.errors) throw res.errors
      }
      return res.data.savePerson
    },
    deletePerson: async (id, tombstoneInput, errorCheck = true) => {
      const res = await deletePerson(id, tombstoneInput)
      if (errorCheck) {
        t.error(res.errors, 'deletes person without error')
        if (res.errors) throw res.errors
      }
      return res.data.deletePerson
    },
    getPerson: async (id, errorCheck = true) => {
      const res = await getPerson(id)
      if (errorCheck) {
        t.error(res.errors, 'gets person without error')
        if (res.errors) throw res.errors
      }
      return res.data.person
    }
  }
}

test('kaitiaki tombstones a person/group profile they created', async t => {
  t.plan(7)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot()
  const {
    initGroup,
    savePerson,
    deletePerson,
    getPerson
  } = TestbotHelpers(kaitiaki, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group
  const { groupId } = await initGroup({})

  // 2. kaitiaki creates a person/group profile
  const profileInput = {
    type: 'person',
    authors: {
      add: [
        '*'
      ]
    },
    recps: [groupId]
  }
  const profileId = await savePerson(profileInput)

  // 3. kaitiaki gets the profile
  let profile = await getPerson(profileId)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null,
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile'
  )

  // 4. kaitiaki tombstones the profile
  const tombstonedProfileId = await deletePerson(profileId, { reason: 'kaitiaki deleted' })
  t.deepEqual(tombstonedProfileId, profileId, 'returns the profileId')

  // // 5. kaitiaki gets the profile
  profile = await getPerson(profileId)

  t.deepEqual(
    profile.tombstone,
    {
      reason: 'kaitiaki deleted'
    },
    'returns tombstone'
  )

  kaitiaki.close()
})

test('kaitiaki tombstones a person/admin profile they created', async t => {
  t.plan(7)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot()
  const {
    initGroup,
    savePerson,
    deletePerson,
    getPerson
  } = TestbotHelpers(kaitiaki, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group
  const { poBoxId } = await initGroup({})

  // 2. kaitiaki creates a person/group/admin profile
  const profileInput = {
    type: 'person/admin',
    authors: {
      add: [
        '*'
      ]
    },
    recps: [poBoxId, kaitiaki.id]
  }
  const profileId = await savePerson(profileInput)

  // 3. kaitiaki gets the profile
  let profile = await getPerson(profileId)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null,
      tombstone: null,
      recps: [poBoxId, kaitiaki.id]
    },
    'returns correct profile'
  )

  // 4. kaitiaki tombstones the profile
  const tombstonedProfileId = await deletePerson(profileId, { reason: 'kaitiaki deleted' })
  t.deepEqual(tombstonedProfileId, profileId, 'returns the profileId')

  // // 5. kaitiaki gets the profile
  profile = await getPerson(profileId)

  t.deepEqual(
    profile.tombstone,
    {
      reason: 'kaitiaki deleted'
    },
    'returns tombstone'
  )

  kaitiaki.close()
})

test('kaitiaki tombstones a person/group profile that is linked to a person/admin profile', async t => {
  t.plan(7)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot()
  const {
    initGroup,
    savePerson,
    deletePerson,
    getPerson
  } = TestbotHelpers(kaitiaki, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group
  const { groupId } = await initGroup({})

  // 2. kaitiaki creates a person/group profile
  const profileInput = {
    type: 'person/group',

    preferredName: 'Cherese',
    email: 'cherese@email.com', // admin field means it will create an admin profile and link it
    authors: {
      add: [
        '*'
      ]
    },
    recps: [groupId]
  }
  const profileId = await savePerson(profileInput)

  // 3. kaitiaki gets the profile
  let profile = await getPerson(profileId)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: profile.adminProfile.id, // hack: we dont have this
        email: 'cherese@email.com',
        tombstone: null
      },
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile'
  )

  // 4. kaitiaki tombstones the profile
  const tombstonedProfileId = await deletePerson(profileId, { reason: 'kaitiaki deleted' })
  t.deepEqual(tombstonedProfileId, profileId, 'returns the profileId')

  // // 5. kaitiaki gets the profile
  profile = await getPerson(profileId)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: profile.adminProfile.id,
        email: 'cherese@email.com',
        tombstone: {
          reason: 'kaitiaki deleted'
        }
      },
      tombstone: {
        reason: 'kaitiaki deleted'
      },
      recps: [groupId]
    },
    'returns tombstoned profiles'
  )

  kaitiaki.close()
})

test('kaitiaki tombstones a person/admin profile that is linked to a person/group profile', async t => {
  t.plan(10)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot()
  const {
    initGroup,
    savePerson,
    deletePerson,
    getPerson
  } = TestbotHelpers(kaitiaki, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group
  const { groupId, adminGroupId } = await initGroup({})

  // 2. kaitiaki creates a person/group profile
  const profileInput = {
    type: 'person/group',

    preferredName: 'Cherese',
    email: 'cherese@email.com', // admin field means it will create an admin profile and link it
    authors: {
      add: [
        '*'
      ]
    },
    recps: [groupId]
  }
  const profileId = await savePerson(profileInput)

  // 3. kaitiaki gets the profile
  let profile = await getPerson(profileId)
  const adminProfileId = profile.adminProfile.id

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: adminProfileId, // hack: we dont have this
        email: 'cherese@email.com',
        tombstone: null
      },
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile'
  )

  // 4. kaitiaki tombstones the ADMIN profile
  const tombstonedProfileId = await deletePerson(adminProfileId, { reason: 'kaitiaki deleted' })
  t.deepEqual(tombstonedProfileId, adminProfileId, 'returns the admin profile id')
  t.notEqual(adminProfileId, profile.id, 'admiProfileId different to profileId')

  // // 5. kaitiaki gets the group profile
  profile = await getPerson(profileId)

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: adminProfileId,
        email: 'cherese@email.com',
        tombstone: {
          reason: 'kaitiaki deleted'
        }
      },
      tombstone: {
        reason: 'kaitiaki deleted'
      },
      recps: [groupId]
    },
    'returns tombstoned profiles'
  )

  profile = await getPerson(adminProfileId)

  t.deepEqual(
    profile,
    {
      id: adminProfileId,
      adminProfile: null,
      tombstone: {
        reason: 'kaitiaki deleted'
      },
      recps: [adminGroupId]
    },
    'returns tombstoned admin profile'
  )

  kaitiaki.close()
})

test('member tombstones a person/group profile they created', async t => {
  t.plan(7)

  /**
   * SETUP
   */
  const kaitiaki = await TestBot({ name: 'kaitiaki' })
  const member = await TestBot({ name: 'member' })

  const {
    initGroup
  } = TestbotHelpers(kaitiaki, t)

  const {
    savePerson,
    deletePerson,
    getPerson
  } = TestbotHelpers(member, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group and invites the member to join
  const { groupId } = await initGroup({})
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)
  await sleep(500)

  // 2. member creates a person/group profile
  const profileInput = {
    type: 'person',
    authors: {
      add: [
        '*'
      ]
    },
    recps: [groupId]
  }
  const profileId = await savePerson(profileInput)

  // 3. member gets the profile
  let profile = await getPerson(profileId)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null,
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile to member'
  )

  // 4. member tombstones the profile
  const tombstonedProfileId = await deletePerson(profileId, { reason: 'member deleted' })
  t.deepEqual(tombstonedProfileId, profileId, 'returns the profileId')

  // // 5. member gets the profile
  profile = await getPerson(profileId)

  t.deepEqual(
    profile.tombstone,
    {
      reason: 'member deleted'
    },
    'returns tombstone'
  )

  kaitiaki.close()
  member.close()
})

test('member tombstones profiles the kaitiaki created', async t => {
  t.plan(12)
  // /**
  //  * SETUP
  //  */
  const kaitiaki = await TestBot({ name: 'kaitiaki2' })
  const member = await TestBot({ name: 'member2' })

  const {
    initGroup,
    savePerson: savePersonKaitiaki,
    getPerson: getPersonKaitiaki
  } = TestbotHelpers(kaitiaki, t)

  const {
    getPerson: getPersonMember,
    deletePerson
  } = TestbotHelpers(member, t)

  /**
   * TESTS
   */

  // 1. kaitiaki creates a group and invites the member to join
  const { groupId } = await initGroup({})
  await p(kaitiaki.ssb.tribes.invite)(groupId, [member.ssb.id], {})
  await kaitiaki.replicate(member)
  await member.replicate(kaitiaki)
  await sleep(500)

  // 2. kaitiaki creates a person/group profile
  const profileInput = {
    type: 'person',
    preferredName: 'Cherese',
    email: 'cherese@email.com', // add this field so it creates and links and admin profile
    authors: {
      add: [
        '*'
      ]
    },
    recps: [groupId]
  }
  const profileId = await savePersonKaitiaki(profileInput)
  await kaitiaki.replicate(member)
  // await sleep(500)

  // 3. kaitiaki gets the profile
  let profile = await getPersonKaitiaki(profileId)
  const adminProfileId = profile.adminProfile.id

  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: adminProfileId,
        email: 'cherese@email.com',
        tombstone: null
      },
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile to kaitiaki'
  )

  // 4. member gets the profile
  profile = await getPersonMember(profileId)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null, // member cannot see the admin profile
      tombstone: null,
      recps: [groupId]
    },
    'returns correct profile to member'
  )

  // 5. member tries to tombstone the admin profile
  await deletePerson(adminProfileId, { reason: 'member deleted' }, false)
    .then(() => t.fail('member shouldnt be able to tombstone the admin profile'))
    .catch(() => t.pass('member couldnt tombstone the admin profile'))

  // 6. member tries to tombstone the group profile
  const tombstonedProfileId = await deletePerson(profileId, { reason: 'member deleted profile' })
  await member.replicate(kaitiaki)

  t.equal(tombstonedProfileId, profileId, 'the group profile was tombstoned')

  // 7. member gets the profile
  profile = await getPersonMember(profileId)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: null, // member cannot see the admin profile
      tombstone: {
        reason: 'member deleted profile'
      },
      recps: [groupId]
    },
    'returns correct tombstoned profile to member'
  )

  // 8. kaitiaki gets the profile
  // TODO: this case needs more discussion!
  profile = await getPersonKaitiaki(profileId)
  t.deepEqual(
    profile,
    {
      id: profileId,
      adminProfile: {
        id: adminProfileId,
        email: 'cherese@email.com',
        tombstone: null
      },
      tombstone: {
        reason: 'member deleted profile'
      },
      recps: [groupId]
    },
    'kaitiaki sees tombstoned group profile but not a tombstoned admin profile'
  )

  kaitiaki.close()
  member.close()
})

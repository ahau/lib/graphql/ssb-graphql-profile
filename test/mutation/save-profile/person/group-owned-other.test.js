const test = require('tape')
const pull = require('pull-stream')
const { promisify: p } = require('util')
const { where, author, live, toPullStream } = require('ssb-db2/operators')

const TestBot = require('../../../test-bot')
const { SavePerson } = require('../../../lib/helpers')

function SaveProfile (apollo, t) {
  const savePerson = SavePerson(apollo)
  return async function savePersonGroup (input) {
    const res = await savePerson(input)

    t.error(res.errors, input.id ? 'savePerson returned no errors for update' : 'savePerson return no errors for create')

    return res.data.savePerson
  }
}

// this checks what happens when an admin tries to update a group profile they don't have authorship on
// we expect all changes to be saved to a adminProfile
//
// CASES:
// 1. savePerson - person.group.update (owned, no adminProfile yet)
// 2. savePerson - person.group.update (owned, existing adminProfile)
// 3. savePerson - person.group.update (owned, ypu are not an admin)
//
// NOTE: "owned" in this case means someone else has authorship rights on this
// There is usually a link/feed-profile present too, but we're just gonna lean on `profile.authors` to test ownership

test('savePerson - person.group.update (owned, no adminProfile yet)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // TODO we need to add the linkages usually here for a connected / owned
  // we don't expect a link/profile-profile/admin to be made, instead there should be
  // link/feed-profiles that get us the admin profile
  //
  // ALTERNATIVE - change app to publish link/profile-profile/admin for every adminProfile ... to make lookup easier

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: subGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true })

  // profile someone else constrols
  const memberId = '@ICykrdqOhbUaAcGqRX0NFFqW6M3PabOWe7PYr0K4rSQ=.ed25519'
  const groupProfileId = await p(ssb.profile.person.group.create)({
    authors: { add: [memberId] }, // so admin cannot edit!
    recps: [groupId]
  })
  await p(ssb.profile.link.create)(groupProfileId, { feedId: memberId })

  const publishedMsgs = []

  const stream = new Promise((resolve) => {
    pull(
      ssb.db.query(
        where(author(ssb.id)),
        live({ old: false }),
        toPullStream()
      ),
      pull.take(2),
      pull.drain(m => {
        publishedMsgs.push(m)

        if (publishedMsgs.length < 2) return
        // expect
        // 0 = adminProfile creation
        // 1 = link/profile-profile/admin

        // // ////
        // // check it updated the unowned person profile in the admin group
        // // ////
        const adminProfileId = publishedMsgs[0].key
        const adminProfile = publishedMsgs[0].value.content

        t.deepEqual(
          {
            type: adminProfile.type,
            preferredName: adminProfile.preferredName?.set,
            phone: adminProfile.phone?.set,
            root: adminProfile?.tangles?.profile?.root,
            recps: adminProfile.recps
          },
          {
            type: 'profile/person/admin',
            preferredName: 'Alice', // NOTE: saves name here as can't save to owned profile
            phone: '2717777777777',
            root: null,
            recps: [subGroupId]
          },
          'the admin profile was updated'
        )

        // ////
        // check it created a link for the two profiles
        // ////
        const link = publishedMsgs?.[1]?.value?.content || {}
        t.deepEqual(
          {
            type: link.type,
            parent: link.parent,
            child: link.child,
            recps: link.recps
          },
          {
            type: 'link/profile-profile/admin',
            parent: adminProfileId,
            child: groupProfileId,
            recps: [groupId]
          },
          'link for group-admin profiles created'
        )

        resolve()
      })
    )
  })
  // ////
  // update the profile!
  // ////
  const updateInput = {
    id: groupProfileId,
    preferredName: 'Alice',
    phone: '2717777777777' // << for admins only!
  }

  await savePersonGroup(updateInput)

  await stream

  await p(ssb.close)(true)

  t.end()
})

test('savePerson - person.group.update (owned, existing adminProfile)', async (t) => {
  const { ssb, apollo } = await TestBot()
  const savePersonGroup = SaveProfile(apollo, t)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  const { poBoxId } = await p(ssb.tribes.subtribe.create)(groupId, { admin: true, addPOBox: true })

  // profile someone else controls - set up via (simulated) application process
  const memberId = '@ICykrdqOhbUaAcGqRX0NFFqW6M3PabOWe7PYr0K4rSQ=.ed25519'
  const groupProfileId = await p(ssb.profile.person.group.create)({
    authors: { add: [memberId] }, // so admin cannot edit!
    recps: [groupId]
  })
  const adminProfileId = await p(ssb.profile.person.admin.create)({
    authors: { add: ['*'] },
    recps: [poBoxId, memberId]
  })
  await p(ssb.profile.link.create)(groupProfileId, { feedId: memberId })
  await p(ssb.profile.link.create)(adminProfileId, { feedId: memberId })

  // update the profile!
  const updateInput = {
    id: groupProfileId,
    preferredName: 'Alice',
    phone: '2717777777777' // << for admins only!
  }
  await savePersonGroup(updateInput)

  await new Promise((resolve) => {
    pull(
      ssb.db.query(
        where(author(ssb.id)),
        toPullStream()
      ),
      pull.collect(
        (err, msgs) => {
          if (err) throw err

          const m = msgs[msgs.length - 1]

          // ////
          // check it updated the unowned person profile in the admin group
          // ////
          const adminProfile = m.value.content

          t.deepEqual(
            {
              root: adminProfile.tangles.profile.root,
              type: adminProfile.type,
              preferredName: adminProfile.preferredName?.set,
              phone: adminProfile.phone?.set,
              isUpdate: typeof adminProfile?.tangles?.profile?.root === 'string',
              recps: adminProfile.recps
            },
            {
              root: adminProfileId,
              type: 'profile/person/admin',
              preferredName: 'Alice', // NOTE: saves name here as can't save to owned profile
              phone: '2717777777777',
              isUpdate: true,
              recps: [poBoxId, memberId]
            },
            'the admin profile was updated'
          )
          resolve()
        }
      )
    )
  })
  ssb.close()
  t.end()
})

test('savePerson - person.group.update (owned, you are not an admin)', async (t) => {
  // expect that you won't be able to edit the group profile, and your whole update should fail
  // as you have no-where to save the update to...

  const { ssb, apollo } = await TestBot()
  const savePerson = SavePerson(apollo)

  // ////
  // Init the group
  // ////
  const { groupId } = await p(ssb.tribes.create)({})
  // NOTE - no admin group! // simulating us not being an admin

  // profile someone else constrols

  const feedId = '@ICykrdqOhbUaAcGqRX0NFFqW6M3PabOWe7PYr0K4rSQ=.ed25519'
  const groupProfileId = await p(ssb.profile.person.group.create)({
    authors: { add: [feedId] }, // so admin cannot edit!
    recps: [groupId]
  })
  await p(ssb.profile.link.create)(groupProfileId, { feedId })

  // ////
  // update the profile!
  // ////
  const updateInput = {
    id: groupProfileId,
    preferredName: 'Alice',
    phone: '2717777777777' // << for admins only!
  }
  const result = await savePerson(updateInput)

  t.match(result.err.message, /no admin subgroup found/, 'cannot save update anywhere')

  ssb.close()
  t.end()
})

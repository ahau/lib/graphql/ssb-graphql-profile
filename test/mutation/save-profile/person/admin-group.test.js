const test = require('tape')
const { promisify: p } = require('util')

const TestBot = require('../../../test-bot')
const { SavePerson } = require('../../../lib/helpers')

test('savePerson (admin group)', async (t) => {
  const { ssb, apollo } = await TestBot({ loadContext: true, recpsGuard: true })
  const savePerson = SavePerson(apollo)

  // create a group with an admin subgroup
  const { groupId } = await p(ssb.tribes.create)({})
  const { groupId: adminSubGroupId } = await p(ssb.tribes.subtribe.create)(groupId, { addPOBox: true, admin: true })

  // try and encrypt a profile to the admin group

  const profileInput = {
    type: 'person/admin',
    preferredName: 'Cherese',
    // adminProfile details
    email: 'cherese@email.com',

    // required details
    authors: {
      add: ['*']
    },
    recps: [adminSubGroupId] // NOTE: this is what the front-end is doing
  }

  const res = await savePerson(profileInput)

  t.error(res.errors, 'saves profile without error')

  ssb.close()
  t.end()
})

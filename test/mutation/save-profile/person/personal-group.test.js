const test = require('tape')

const TestBot = require('../../../test-bot')
const { SavePerson } = require('../../../lib/helpers')

test('savePerson (personal group)', async (t) => {
  // t.plan(13)
  const { ssb, apollo } = await TestBot({ loadContext: true, recpsGuard: true })
  const savePerson = SavePerson(apollo)

  let res = await apollo.query({
    query: `{
      whoami {
        personal {
          groupId
        }
      }
    }`
  })

  t.error(res.errors, 'query whoami without error')

  // try and encrypt a profile to your personal group
  const groupId = res.data.whoami.personal.groupId

  const profileInput = {
    type: 'person',
    preferredName: 'Cherese',
    // adminProfile details
    email: 'cherese@email.com',

    // required details
    authors: {
      add: ['*']
    },
    recps: [groupId] // NOTE: this is what the front-end is doing
  }

  res = await savePerson(profileInput)
  // NOTE - email was not persisted here (there is not admin subgroup)

  t.error(res.errors, 'saves profile without error')

  ssb.close()
  t.end()
})

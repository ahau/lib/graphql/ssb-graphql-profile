const { isMsg, isFeed, isCloakedMsg } = require('ssb-ref')
const { GraphQLError } = require('graphql')

const simplify = require('../../lib/simplify-profile')

const MSG = 'msg'
const FEED = 'feed'
const GROUP = 'group'

module.exports = function GetProfile (sbot, cache) {
  const legacyCrut = LegacyProfileCrut(sbot)

  const feedToMsg = {}
  const groupToMsg = {}

  return function getProfile (id, cb) {
    // NOTE id can be :
    //   - MsgId = the id of the profile itself
    //   - FeedId = we map this to the a linked Public Profile
    //   - * = this is from ssb-crut-authors and has no profile
    if (id === '*') return cb(null, null)

    let type
    if (isMsg(id)) type = MSG
    else if (isFeed(id)) type = FEED
    else if (isCloakedMsg(id)) type = GROUP
    else {
      return cb(
        new GraphQLError(
          'profile query expected %msgId, @feedId or %cloakedMsg, got ' + id
        )
      )
    }

    // check cache for copy of associated profile
    let profileId
    if (type === MSG) profileId = id
    if (type === FEED) profileId = feedToMsg[id]
    if (type === GROUP) profileId = groupToMsg[id]

    const cached = cache.get(profileId) // only use MsgId keys
    if (cached) {
      return cb(null, cached)
    }

    // otherwise look one up
    profileFromDb(id, (err, state) => {
      if (err) return cb(new GraphQLError(err))

      if (!state) return cb(null, null)

      cache.set(state.id, state)
      if (type === FEED) feedToMsg[id] = state.id
      if (type === GROUP) groupToMsg[id] = state.id

      cb(null, state)
    })
  }

  function profileFromDb (id, cb) {
    if (isMsg(id)) legacyCrut.get(id, done)
    else {
      const getter = isFeed(id)
        ? sbot.profile.findByFeedId
        : sbot.profile.findByGroupId // safe as checked id type above

      getter(id, (err, profiles) => {
        if (err) return cb(new GraphQLError(err))

        // WARNING! We're using the first public profile for now
        const publicProfile = profiles.public && profiles.public[0]
        if (!publicProfile) {
          // console.error(new GraphQLError(`unable to find public profile associated with ${id}`))
          return cb(null, null)
        }

        done(null, publicProfile)
      })
    }

    function done (err, profile) {
      if (err) cb(new GraphQLError(err))
      else cb(null, simplify(profile))
    }
  }
}

// subset of ssb-profile/legacy
function LegacyProfileCrut (ssb) {
  return {
    get (profileId, cb) {
      loadCrut(profileId, (err, crut) => {
        if (err) return cb(new GraphQLError(err))
        crut.get(profileId, cb)
      })
    }
  }

  /* private */

  function loadCrut (profileId, cb) {
    ssb.get({ id: profileId, private: true }, (err, value) => {
      if (err) return cb(new GraphQLError(err))

      if (typeof value.content === 'string') return cb(new GraphQLError('cannot decrypt this profile'))
      const crut = matchCrut(value.content.type, value.content.recps)

      if (crut) cb(null, crut)
      else cb(new GraphQLError(`No profile helpers for type ${value.content.type} with recps ${value.content.recps}`))
    })
  }
  function matchCrut (type, recps) {
    switch (type) {
      case 'profile/person':
        return recps
          ? ssb.profile.person.group
          : ssb.profile.person.public

      case 'profile/person/source':
        return ssb.profile.person.source

      case 'profile/person/admin':
        return ssb.profile.person.admin

      case 'profile/community':
        return recps
          ? ssb.profile.community.group
          : ssb.profile.community.public

      case 'profile/pataka':
        if (!recps) return ssb.profile.pataka.public
    }
  }
}

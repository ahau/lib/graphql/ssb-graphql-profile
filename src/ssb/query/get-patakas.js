const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { GraphQLError } = require('graphql')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function GetPatakas (ssb, getProfile) {
  function pullPatakaFeedIds () {
    return pull(
      ssb.db.query(where(type('profile/pataka')), toPullStream()),
      pull.map(m => m.value.author),
      pull.unique()
    )
  }

  return function getPataka (hops = 2, cb) {
    // load the graph of people following me (out to "hops" hops)
    ssb.friends.hops({ start: ssb.id, reverse: true, max: hops }, (err, graph) => {
      if (err) cb(new GraphQLError(err))

      pull(
        pullPatakaFeedIds(),
        // keep those not blocking me, and within "hops" range folling me
        pull.filter(feedId => graph[feedId] > 0 && graph[feedId] <= hops),
        paraMap(
          (feedId, cb) => {
            getProfile(feedId, (err, profile) => {
              if (err) return cb(new GraphQLError(err))
              ssb.friends.isBlocking({ source: ssb.id, dest: feedId }, (err, isBlocked) => {
                if (err) return cb(new GraphQLError(err))

                cb(null, {
                  feedId,
                  ...profile,
                  isBlocked
                })
              })
            })
          },
          4
        ),
        pull.collect((err, data) => {
          if (err) return cb(new GraphQLError(err))

          cb(null, data)
        })
      )
    })
  }
}

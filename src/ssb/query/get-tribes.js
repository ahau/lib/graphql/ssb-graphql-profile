const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { GraphQLError } = require('graphql')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function GetTribes (sbot, getTribe) {
  function pullGroupIds () {
    return pull(
      sbot.db.query(where(type('link/group-profile')), toPullStream()),
      pull.filter(m => m.value?.content?.tangles?.link?.root === null),
      pull.filter(m => !m.value.content.parentGroupId), // only include those that dont have parentGroupId (those are links for subgroup profiles)
      pull.map(m => m.value.content.parent),
      pull.unique(),
      pull.filter(Boolean)
    )
  }

  return function getTribes (cb) {
    pull(
      pullGroupIds(),
      paraMap(getTribe, 6),
      pull.filter(tribe => tribe.public.length + tribe.private.length),
      // if a tribe has neither public NOR group profiles, then it's been "deleted"
      pull.collect((err, data) => {
        if (err) return cb(new GraphQLError(err))

        cb(null, data)
      })
    )
  }
}

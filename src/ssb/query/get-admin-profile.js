const pull = require('pull-stream')
const { promisify: p } = require('util')
const { GraphQLError } = require('graphql')
const { where, type, toPullStream } = require('ssb-db2/operators')

module.exports = function GetAdminProfile (ssb, getProfile, cache) {
  return async function getAdminProfile (groupId, groupProfile) {
    const cacheKey = groupProfile.id

    const profileId = cache.get(cacheKey)
    if (profileId === null) return null // have already failed to find
    if (profileId) return p(getProfile)(profileId) // have found previously, use getProfile (which has a cache)

    let profile
    // groupProfile <-- feedId --> adminProfile
    profile = await findViaFeedId(groupId, groupProfile)
      .catch(err => console.error(err))

    if (!profile) {
      // groupProfile --> adminProfile
      profile = await findViaDirectLink(groupId, groupProfile)
        .catch(err => console.error(err))
    }

    cache.set(cacheKey, profile ? profile.id : null)
    return profile
  }

  async function findViaFeedId (groupId, groupProfile) {
    // need to find the feed the parent profile is linked to,
    // then the admin profile will also be linked to that feed
    const feed = await p(findFeedByProfileId)(groupProfile.id)
    if (!feed) return null

    // use the groupId to look for the poBoxId of the admin subgroup
    const poBoxId = await p(findAdminGroupPoBoxId)(groupId)
    if (!poBoxId) return null

    // use the poBox to look for the profile
    return await p(findProfileByFeedInGroup)(feed, poBoxId)
  }
  function findFeedByProfileId (profileId, cb) {
    pull(
      ssb.db.query(
        where(type('link/feed-profile')),
        toPullStream()
      ),
      pull.filter(msg => msg.value.content.child === profileId),
      pull.filter(msg => msg.value.content.tangles.link.root === null),
      pull.filter(msg => msg.value.content.tangles.link.previous === null),
      pull.map(link => link.value.content.parent),
      pull.take(1),
      pull.collect((err, feeds) => {
        if (err) return cb(new GraphQLError(err))

        if (!feeds?.length) return cb(null, null)
        cb(null, feeds[0])
      })
    )
  }
  function findAdminGroupPoBoxId (groupId, cb) {
    ssb.tribes.findSubGroupLinks(groupId, (err, subGroupLinks) => {
      if (err) return cb(new GraphQLError(err))

      const admin = subGroupLinks.filter(link => link.admin)

      if (admin.length === 0) {
        console.error('found group without an admin subgroup')
        return cb(null, null)
      }

      ssb.tribes.poBox.get(admin[0].subGroupId, (err, data) => {
        if (err) {
          if (err.message.match(/unknown groupId/)) return cb(null, null)
          else return cb(new GraphQLError(err))
        }

        cb(null, data.poBoxId)
      })
    })
  }

  function findProfileByFeedInGroup (feedId, groupId, cb) {
    ssb.profile.findByFeedId(feedId, { selfLinkOnly: false, groupId, getProfile }, (err, profiles) => {
      if (err) return cb(new GraphQLError(err))

      if (profiles.self.private.length) {
        return cb(null, profiles.self.private[0])
      }

      if (profiles.other.private.length) {
        return cb(null, profiles.other.private[0])
      }

      cb(null, null)
    })
  }

  function findViaDirectLink (groupId, groupProfile) {
    return new Promise((resolve, reject) => {
      pull(
        ssb.db.query(
          where(type('link/profile-profile/admin')),
          toPullStream()
        ),
        pull.filter(msg => msg.value.content.child === groupProfile.id),
        pull.filter(msg => msg.value.content.tangles.link.root === null),
        pull.filter(msg => msg.value.content.tangles.link.previous === null),
        pull.filter(msg => msg.value.content.recps.length === 1),
        pull.filter(msg => msg.value.content.recps[0] === groupId),
        pull.map(link => link.value.content.parent),
        pull.take(1),
        pull.collect((err, feeds) => {
          if (err) return reject(err)

          if (!feeds?.length) return resolve(null)

          getProfile(feeds[0], (err, profile) => err ? reject(err) : resolve(profile))
        })
      )
    })
  }
}

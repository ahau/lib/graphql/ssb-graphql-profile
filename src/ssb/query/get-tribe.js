const { GraphQLError } = require('graphql')

module.exports = function GetTribe (sbot, getProfile) {
  const getActiveProfile = (profileId, cb) => {
    getProfile(profileId, (err, profile) => {
      if (err) return cb(null, null)
      if (profile.tombstone) return cb(null, null)

      cb(null, profile)
    })
  }

  return function getTribe (id, cb) {
    sbot.profile.findByGroupId(id, { getProfile: getActiveProfile }, (err, profiles) => {
      if (err) return cb(new GraphQLError(err))

      const canEdit = profiles.private && profiles.private.length > 0

      profiles.private = profiles.private.map(d => mapProfiles(d, canEdit))
      profiles.public = profiles.public.map(d => mapProfiles(d, canEdit))

      cb(null, { id, ...profiles })
    })
  }
}

function mapProfiles (d, canEdit) {
  return {
    ...d,
    canEdit
  }
}

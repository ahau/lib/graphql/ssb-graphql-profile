const { GraphQLError } = require('graphql')

module.exports = function FindPersons (sbot) {
  return function findPersons (name, { type, groupId }, cb) {
    if (type === undefined) type = 'person'
    sbot.profile.find({ type, name, groupId, decorateWithLinkedFeeds: true }, (err, profiles) => {
      if (err) return cb(new GraphQLError(err))

      if (!profiles) return cb(null, [])

      const seenGroupProfiles = new Set()

      profiles = profiles
        // make sure the next step doesnt filter out the first profile made
        // thats the one we always want over duplicate ones
        .filter(profile => {
          const alreadyLinked = profile.linkedFeeds.some(feedId => seenGroupProfiles.has(groupProfileId(profile, feedId)))
          profile.linkedFeeds.forEach(feedId => seenGroupProfiles.add(groupProfileId(profile, feedId)))
          // NOTE 2021-04-21 mix
          // not quite sure what the best behaviour here is.
          // Currently there's an edge case where if you have the following profiles, MixB will be
          // dropped with the current logic (if MixA was processed first)
          //
          //   MixA = { linkedFeeds: [feedA, feedB] }
          //   MixB = { linkedFeeds: [feedB] }

          return !alreadyLinked
        })
        .map(profile => {
          profile.id = profile.key
          return profile
        })

      cb(null, profiles)
    })
  }
}

function groupProfileId (profile, feedId) {
  const recps = profile.recps || []
  return feedId + JSON.stringify(recps.sort()) // guarantee deterministic ids
}

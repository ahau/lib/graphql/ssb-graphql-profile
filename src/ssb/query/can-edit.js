const { GraphQLError } = require('graphql')
const isActiveAuthor = require('../../lib/is-active-author')

const ALL_AUTHORS = '*'

module.exports = function CanEdit (sbot) {
  const feedId = sbot.id

  return function canEdit (authors, cb) {
    if (!authors) return cb(new GraphQLError('the profile returned is missing authors'))

    if (isActiveAuthor(authors[ALL_AUTHORS], Date.now())) return cb(null, true)

    sbot.getFeedState(feedId, (err, state) => {
      if (err) return cb(new GraphQLError(err))

      cb(null, isActiveAuthor(authors[feedId], state.sequence))
    })
  }
}

const pull = require('pull-stream')
const paraMap = require('pull-paramap')
const { GraphQLError } = require('graphql')

const noop = () => {}

module.exports = function GetConnectedPeers (sbot, getProfile) {
  const preLoadProfile = (feedId) => getProfile(feedId, noop)
  let connected

  function initialise () {
    connected = []

    pull(
      sbot.conn.peers(),
      pull.map(entries => entries
        .map(([addr, data]) => data)
        .filter(data => data.state === 'connected')
        .map(data => data.key)
      ),
      pull.drain(feedIds => {
        connected = feedIds

        feedIds.forEach(preLoadProfile)
      })
    )
  }

  return function getConnectedPeers (cb) {
    if (connected === undefined) initialise()

    const profiles = {
      person: [],
      pataka: []
    }

    pull(
      pull.values(Array.from(connected)),
      paraMap(
        (feedId, cb) => {
          getProfile(feedId, (err, profile) => {
            if (err) {
              console.error(`getConnectedPeers: could not load profile ${feedId}`)
              console.error(err)
              return cb(null, null)
            }

            cb(null, profile)
          })
        },
        5 // width of paraMap
      ),
      pull.filter(Boolean),
      pull.through(profile => {
        if (!profiles[profile.type]) profiles[profile.type] = []
        profiles[profile.type].push(profile)
      }),
      pull.collect((err) => {
        if (err) return cb(new GraphQLError(err))

        cb(null, profiles)
      })
    )
  }
}

const { GraphQLError } = require('graphql')
const simplify = require('../../lib/simplify-profile')

module.exports = function ListPerson (ssb, cache) {
  return function listPerson (type, groupId, cb) {
    const crut = ssb.profile.person[type]
    if (!crut) return cb(new GraphQLError(`invalid type '${type}', expected one of [public, group, admin, source]`))

    const read = (key, cb) => {
      if (typeof key !== 'string') key = key.key

      /* cache check */
      const cached = cache.get(key)
      if (cached) {
        return cb(null, cached)
      }

      crut.get(key, (err, record) => {
        // if (err) console.log(err)
        if (err) return cb(null, null) // prevent an error being thrown, ignore this record

        const person = simplify(record)
        cache.set(key, person)

        cb(null, person)
      })
    }

    crut.list({ groupId, read }, cb)
  }
}

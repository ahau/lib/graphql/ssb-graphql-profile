const pull = require('pull-stream')
const { GraphQLError } = require('graphql')

module.exports = function GetTiaki (sbot) {
  return function getTiaki (authors, cb) {
    pull(
      pull.values(authors),
      pull.asyncMap((author, cb) => {
        sbot.profile.findByFeedId(author, cb)
      }),
      pull.collect((err, profiles) => {
        if (err) return cb(new GraphQLError(err))

        profiles = profiles.map(profile => {
          const state = profile.public[0].states[0]

          return {
            id: profile.public[0].key,
            feedId: profile.public[0].feedId,
            // WARNING! we're assuming just one head-state!
            ...state
          }
        })
        cb(null, profiles)
      })
    )
  }
}

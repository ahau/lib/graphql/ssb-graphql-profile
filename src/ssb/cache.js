const Cache = require('hashlru')
const pull = require('pull-stream')
const pullMany = require('pull-many')
const { where, isDecrypted, live: dbLive, toPullStream } = require('ssb-db2/operators')

module.exports = function (ssb) {
  const cache = {
    profile: new Cache(1000), //  maps profileId --> profile|null
    adminLink: new Cache(4000) // maps groupProfileId --> adminProfileId|null
  }

  cacheInvalidator(ssb, cache)

  return cache
}

function cacheInvalidator (ssb, cache) {
  ssb.db.reindexEncrypted.hook((rebuild, [cb]) => {
    cache.adminLink.clear()
    rebuild(cb)
  })

  const typesToWatch = new Set([
    'profile/person',
    'profile/person/source',
    'profile/person/admin',
    'profile/community',
    'profile/pataka',
    'link/feed-profile',
    'link/profile-profile/admin'
  ])

  pull(
    pullMany([
      ssb.db.query(
        where(isDecrypted('box2')),
        dbLive({ old: false }),
        toPullStream()
      ),
      ssb.db.reindexed()
    ]),
    pull.filter(m => typesToWatch.has(m.value.content.type)),
    pull.map(m => m.value.content),
    pull.drain(content => {
      if (isProfileUpdate(content)) cache.profile.remove(getProfileId(content))
      else if (isLinkMsg(content)) {
        const { parent, child } = content
        if (parent.startsWith('%')) cache.adminLink.remove(parent)
        if (child.startsWith('%')) cache.adminLink.remove(child)
      }
    })
  )

  /* listen to local method calls to quickly invalidate */
  const types = [
    ['person', 'public'],
    ['person', 'group'],
    ['person', 'source'],
    ['person', 'admin'],

    ['community', 'public'],
    ['community', 'group'],

    ['pataka', 'public']
  ]
  types.forEach(([superType, subType]) => {
    ssb.profile[superType][subType].update.hook(updateProfileCache)
    ssb.profile[superType][subType].tombstone.hook(updateProfileCache)
  })

  /* legacy methods */
  const legacyTypes = [
    'publicPerson',
    'privatePerson',
    'publicCommunity',
    'privateCommunity'
  ]
  legacyTypes.forEach(type => {
    ssb.profile[type].update.hook(updateProfileCache)
    ssb.profile[type].tombstone.hook(updateProfileCache)
  })

  function updateProfileCache (update, args) {
    cache.profile.remove(args[0])
    update(...args)
  }
}

// NOTE - we might get race conditions when a person updates a profile
// as they save a change, which then results in indexing changes...

function isProfileUpdate (content) {
  return (
    content.type.startsWith('profile') &&
    content?.tangles?.profile?.root !== null
  )
}
function isLinkMsg (content) {
  return content.type.startsWith('link')
}

function getProfileId (content) {
  return content?.tangles?.profile?.root
}

const { GraphQLError } = require('graphql')
const fixInput = require('../../lib/fix-input')

module.exports = function PostSaveCommunity (ssb) {
  function getCrut (recps) {
    return recps
      ? ssb.profile.community.group
      : ssb.profile.community.public
  }

  return function postSaveCommunity (input, cb) {
    const { id: profileId } = input
    const details = fixInput.community(input)

    if (!profileId) {
      // ///
      // CREATE
      // ///
      const crut = getCrut(details.recps)
      crut.create(details, cb)
    } else {
      ssb.get({ id: profileId, private: true }, (err, val) => {
        if (err) return cb(new GraphQLError(err))

        // ///
        // UPDATE
        // ///
        const crut = getCrut(val.content.recps)
        crut.update(profileId, details, cb)
      })
    }
  }
}

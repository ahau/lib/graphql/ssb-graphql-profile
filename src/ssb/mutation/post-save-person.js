/* eslint brace-style: ["error", "stroustrup"] */
const spec = require('ssb-profile/spec')
const get = require('lodash.get')
const isEqual = require('lodash.isequal')
const { GraphQLError } = require('graphql')

const PostTombstonePerson = require('./post-tombstone-person')
const BulkUpdateProfiles = require('../../lib/bulk-update-profiles')
const FindPersonAdminProfile = require('../../lib/find-person-admin-profile')
const fixInput = require('../../lib/fix-input')
const getTypePath = require('../../lib/get-type-path')

// Many behaviours here:
//
// is profileId supplied?
//  ├─NO── create group profile,
//  │      then were there any admin-only details?
//  │       └─YES─ create an admin profile (may fail if not admin)
//  │
//  └─YES─ is it one of my profiles? (linked to your feedId)
//           ├─YES─ is it my public profile?
//           │       ├─YES─ update that profile
//           │       └─NO── bulk update all my profiles
//           │
//           └─NO── what type of profile is it?
//                    ├─ADMIN─ update the profile (you can edit if you can see it)
//                    └─GROUP─ can I edit it?
//                              ├─YES─ update it.
//                              │      then were there "admin-only" details
//                              │       └─YES─ are you an admin?
//                              │               ├─YES─ updateOrCreateAdminProfile (admin fields only?)
//                              │               └─NO── ??!! error?
//                              │
//                              └─NO── are you an admin?
//                                      ├─YES─ updateOrCreateAdminProfile (all fields)
//                                      └─NO── ??!! error?
//
// updateOrCreateAdminProfile, this means go looking for a link between this group profile and an admin profile
// is there an adminProfile?
//  ├─YES─ update it
//  └─NO── create one and link it

module.exports = function PostSavePerson (ssb) {
  const bulkUpdateProfiles = BulkUpdateProfiles(ssb)
  const findAdminProfile = FindPersonAdminProfile(ssb)
  const postTombstonePerson = PostTombstonePerson(ssb)

  return function postSavePerson (input, cb) {
    const { type, id: profileId } = input

    const details = fixInput.person(input)

    if (details.tombstone) {
      postTombstonePerson(profileId, details.tombstone, cb)
      return
    }

    if (!profileId) createNewProfile(type, details, cb)
    else {
      ssb.profile.findByFeedId(ssb.id, (err, profiles) => {
        if (err) return cb(new GraphQLError(err))

        // see if the given id matches one of the associated profiles
        const myProfile = (
          profiles.private.find(p => p.key === profileId) ||
          profiles.public.find(p => p.key === profileId)
        )

        // here we skip bulk updating if we are updating
        // a personal profiles custom fields
        const skipBulkUpdate = (
          details.customFields &&
          Object.keys(details).length === 1
        )

        const isMyPublicProfile = Boolean(myProfile && !myProfile.recps)

        const done = (err) => err ? cb(new GraphQLError(err)) : cb(null, profileId)

        if (isMyPublicProfile) ssb.profile.person.public.update(profileId, details, done)
        else if (myProfile && !skipBulkUpdate) bulkUpdateProfiles(profiles, details, done)
        else updateProfile(profileId, details, done)
        // NOTE updateProfile may update an admin-profile too
      })
    }
  }

  function createNewProfile (typeStr, details, cb) {
    if (!typeStr) return cb(new GraphQLError('the type is required to create a profile'))

    const typePath = getTypePath(typeStr, details.recps)

    if (!isEqual(typePath, ['person', 'group'])) {
      const crut = get(ssb.profile, typePath)
      if (!crut || !crut.create) return cb(new GraphQLError(`unknown profile type: ${typeStr}`))

      crut.create(details, (err, profileId) => {
        if (err) cb(new GraphQLError(err))
        else cb(null, profileId)
      })
      return
    }

    const groupId = details?.recps[0]
    if (!groupId) return cb(new GraphQLError('createNewProfile requires recps[0] = groupId'))

    // check the tribe we are saving to
    ssb.tribes.get(groupId, (err, tribe) => {
      if (err) return cb(new GraphQLError(err))

      // NOTE: here we check if we are saving to a subgroup (usually the admin subgroup)
      // and if we are, we save directly

      if (tribe?.parentGroupId) {
        details.authors = { add: ['*'] }
        details.recps = [groupId]

        // WARNING: this creates an admin profile, which is fine while we only have admin subgroups,
        // BUT that may change in the future
        ssb.profile.person.admin.create(details, (err, adminProfileId) => {
          if (err) return cb(new GraphQLError(err))

          cb(null, adminProfileId)
        })
        return
      }

      sortPersonDetails(groupId, details, (err, results) => {
        if (err) return cb(new GraphQLError(err))

        const { groupDetails, adminDetails, unknown } = results

        if (unknown) return cb(new GraphQLError(`unknown person.group fields: ${unknown.join(', ')}`))

        ssb.profile.person.group.create(groupDetails, (err, profileId) => {
          if (err) return cb(new GraphQLError(err))

          if (isEqual(adminDetails, {})) return cb(null, profileId)
          // don't save admin details if they're not needed yet!

          createUnownedAdminProfile(profileId, groupId, adminDetails, (err) => {
            if (err) {
              if (err.message.match(/no admin subgroup found/)) {
                // NOTE an error of this type means the group profile has been saved, but some
                // private details were not able to be persisted
                //
                // this could happen for:
                //   - person/profile in personal group (which does not have an admin subgroup)
                //   - person/profile in a group, where you are not an admin/ the owner
                console.error('could not save admin details, no admin subgroup found')
                cb(null, profileId)
              }
              else if (err.message.match(/unknown groupId/)) {
                // NOTE: an error of this type means the person creating the group profile
                // doesnt have access to the admin subgroup, so they cannot create an admin profile
                // this occurs when creating a profile with custom fields, all custom fields are
                // meant to be saved to the admin profile too. Here we are just ignoring the error
                console.error('could not save admin details, user cant encrupt message to the admin subgroup')
                cb(null, profileId)
              }
              else cb(new GraphQLError(err))
            }
            else cb(null, profileId)
          })
        })
      })
    })
  }

  function createUnownedAdminProfile (groupProfileId, groupId, details, cb) {
    if (
      (Object.keys(details).length === 0) ||
      (Object.keys(details).length === 1 && 'tombstone' in details)
    ) return cb(null, groupProfileId)

    // TODO fix ssb-crut so it won't publish create with just a tombstone
    ssb.tribes.findSubGroupLinks(groupId, (err, links) => {
      if (err) return cb(new GraphQLError(err))

      const adminLinks = links.filter(link => link.admin)
      if (adminLinks.length === 0) return cb(new GraphQLError('no admin subgroup found'))
      if (adminLinks.length > 1) console.error('expected 1 admin group, found', adminLinks.length)

      const adminGroupId = adminLinks[0].subGroupId

      details.authors = { add: ['*'] }
      details.recps = [adminGroupId]
      ssb.profile.person.admin.create(details, (err, adminProfileId) => {
        if (err) return cb(new GraphQLError(err))

        ssb.profile.link.create(groupProfileId, { profileId: adminProfileId }, cb)
        // this makes {
        //   parent: adminProfileId,
        //   child: groupProfileId
        //   recps: [groupId],
        //   ...
        //  }
      })
    })
  }

  function updateProfile (profileId, details, cb) {
    ssb.get({ id: profileId, private: true }, (err, val) => {
      if (err) return cb(new GraphQLError(err))

      const typePath = getTypePath(val.content.type, val.content.recps)

      /* person.group updates */
      if (isEqual(typePath, ['person', 'group'])) {
        const groupId = val.content.recps[0]

        if (!groupId) return cb(new GraphQLError('createNewProfile requires recps[0] = groupId'))

        updatePersonGroupProfile(profileId, groupId, details, (err) => {
          err ? cb(new GraphQLError(err)) : cb(null, profileId)
        })
        return
      }

      const crut = get(ssb.profile, typePath)
      if (!crut || !crut.create) return cb(new GraphQLError(`unknown profile type: ${val.content.type}`))

      crut.update(profileId, details, cb)
    })
  }
  function updatePersonGroupProfile (profileId, groupId, details, cb) {
    sortPersonDetails(groupId, details, (err, results) => {
      if (err) return cb(new GraphQLError(err))

      const { groupDetails, adminDetails, unknown } = results

      if (unknown) return cb(new GraphQLError(`unknown person.group fields: ${unknown.join(', ')}`))

      if (isEmpty(groupDetails)) {
        createOrUpdateAdminProfile(profileId, groupId, adminDetails, cb)
        return
      }

      ssb.profile.person.group.update(profileId, groupDetails, (err) => {
        if (err) {
          if (!err.message.match(/invalid author/i)) return cb(new GraphQLError(err))

          const details = { ...groupDetails, ...adminDetails }

          // can't update group profile, so have a go saving all details to admin profile
          return createOrUpdateAdminProfile(profileId, groupId, details, cb)
        }

        createOrUpdateAdminProfile(profileId, groupId, adminDetails, cb)
      })
    })
  }
  function createOrUpdateAdminProfile (profileId, groupId, details, cb) {
    // profileId = the group profile
    // groupId = the id of the group, this will be used to find the admin group + profile

    if (isEqual(details, {})) return cb(null)

    // don't need to proceed if there are not admin details to save
    findAdminProfile(profileId, groupId, (err, adminProfile) => {
      if (err) {
        // if we get this message, it means the user couldnt decrypt the message
        // it is usually because they are not a kaitiaki and arent in the admin group
        // dont throw errors for this
        // TODO: in the future we could incorporate submissions here
        if (err.message.match(/not a valid profile\/person\/admin/) || err.message.match(/unknown groupId/)) return cb(null, profileId)
        return cb(new GraphQLError(err))
      }

      if (adminProfile?.key) ssb.profile.person.admin.update(adminProfile.key, details, cb)
      else createUnownedAdminProfile(profileId, groupId, details, cb)
    })
  }

  function sortPersonDetails (groupId, details, cb) {
    const results = Object.entries(details).reduce(
      (acc, [prop, value]) => {
        if (skipProps.has(prop)) return acc
        else if (groupProps.has(prop)) acc.groupDetails[prop] = value
        else if (adminProps.has(prop)) acc.adminDetails[prop] = value
        else if (prop === 'tombstone') {
          // need to remove tombstone from skipProps
          acc.groupDetails[prop] = value
          acc.adminDetails[prop] = value
        } // eslint-disable-line
        else {
          acc.unknown = acc.unknown || []
          acc.unknown.push(prop)
        }

        return acc
      },
      { groupDetails: {}, adminDetails: {} }
    )

    if (details.customFields) {
      sortCustomFields(groupId, details.customFields, (err, res) => {
        if (err) return cb(new GraphQLError(err))

        results.groupDetails.customFields = res.groupCustomFields
        results.adminDetails.customFields = res.adminCustomFields

        cb(null, results)
      })
      return
    }

    cb(null, results)
  }

  function sortCustomFields (groupId, customFields, cb) {
    // get the custom field definitions from the community profile of the group this profile
    // is encrypted to
    findCustomFieldDefs(groupId, (err, customFieldDefs) => {
      if (err) return cb(new GraphQLError(err))

      const _customFields = Object.entries((customFields || {})).reduce(
        (acc, [key, value]) => {
          // all fields are added to the admin profile regardless of their visibility
          // and if they have a definition
          const fieldDef = customFieldDefs[key]
          if (fieldDef) {
            acc.adminCustomFields[key] = value
            if (fieldDef.visibleBy === 'members') acc.groupCustomFields[key] = value
          }

          return acc
        },
        { groupCustomFields: {}, adminCustomFields: {} }
      )

      cb(null, _customFields)
    })
  }

  function findCustomFieldDefs (groupId, cb) {
    ssb.profile.findByGroupId(groupId, (err, profiles) => {
      if (err) return cb(new GraphQLError(err))

      const customFieldDefs = get(profiles, 'public[0].customFields', {})
      if (!isEmpty(customFieldDefs)) return cb(null, customFieldDefs)

      // try see if this is an admin subgroup instead
      ssb.tribes.get(groupId, (err, data) => {
        if (err) return cb(new GraphQLError(err))
        if (data?.parentGroupId) return findCustomFieldDefs(data.parentGroupId, cb)
        return cb(null, [])
      })
    })
  }
}

const skipProps = new Set(['allowPublic', 'customFields'])
const groupProps = new Set([...Object.keys(spec.person.group.props), 'authors', 'recps'])
const adminProps = new Set(Object.keys(spec.person.admin.props))

function isEmpty (obj) {
  return Object.keys(obj).length === 0
}

const fixInput = require('../../lib/fix-input')

module.exports = function PostSavePataka (ssb) {
  const crut = ssb.profile.pataka.public
  return function postSavePataka (input, cb) {
    const { id: profileId } = input
    const details = fixInput.pataka(input)

    if (!profileId) crut.create(details, cb)
    else crut.update(profileId, details, cb)
  }
}

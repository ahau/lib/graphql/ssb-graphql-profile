const get = require('lodash.get')
const pick = require('lodash.pick')
const isEqual = require('lodash.isequal')

const { GraphQLError } = require('graphql')

const FindPersonAdminProfile = require('../../lib/find-person-admin-profile')
const getTypePath = require('../../lib/get-type-path')

module.exports = function PostTombstonePerson (ssb) {
  const findAdminProfile = FindPersonAdminProfile(ssb)

  function getProfileInfo (profileId, cb) {
    ssb.get({ id: profileId, private: true }, (err, value) => {
      if (err) return cb(new GraphQLError(err))

      if (typeof value.content === 'string') return cb(new GraphQLError('cannot decrypt this profile'))

      const typePath = getTypePath(value.content.type, value.content.recps)
      const groupId = value.content.recps
        ? value.content.recps[0]
        : null

      return cb(null, { groupId, typePath })
    })
  }

  function findAndTombstoneGroupProfile (profileId, tombstone, done) {
    ssb.profile.person.group.findAdminProfileLinks(profileId, (err, links) => {
      if (err) return done(err)

      if (!links.childLinks.length) return done()
      if (links.childLinks.length > 1) console.error('expected 1 group profile, found', links.parentLinks.length)

      const groupProfileId = links.childLinks[0].child
      ssb.profile.person.group.tombstone(groupProfileId, tombstone, done)
    })
  }

  function findAndTombstoneAdminProfile (adminProfileId, groupId, tombstone, done) {
    if (!groupId) return done()

    findAdminProfile(adminProfileId, groupId, (err, adminProfile) => {
      if (err) {
        // if we get this message, it means the user couldnt decrypt the message
        // it is usually because they are not a kaitiaki and arent in the admin group
        // dont throw errors for this
        // TODO: in the future we could incorporate submissions here
        if (err.message.match(/not a valid profile\/person\/admin/) || err.message.match(/unknown groupId/)) return done()
        return done(err)
      }

      if (adminProfile?.key) ssb.profile.person.admin.tombstone(adminProfile.key, tombstone, done)
      else done()
    })
  }

  return function postTombstonePerson (profileId, input = {}, cb) {
    const tombstone = pick(input, ['date', 'reason'])
    tombstone.date = Number(tombstone.date) || Date.now()

    getProfileInfo(profileId, (err, info) => {
      if (err) return cb(new GraphQLError(err))

      const { groupId, typePath } = info

      const done = (err) => err ? cb(new GraphQLError(err)) : cb(null, profileId)

      if (isEqual(typePath, ['person', 'public'])) {
        // update
        ssb.profile.person.public.update(profileId, { tombstone, allowPublic: true }, done)
        return
      }

      const crut = get(ssb.profile, typePath)
      if (!crut || !crut.tombstone) return done(`unknown profile type ${typePath}`)

      crut.tombstone(profileId, tombstone, (err) => {
        if (err) return done(err)

        switch (true) {
          // if the profile was a person/group, we also want to check for a
          // linked admin profile and delete that too
          case isEqual(typePath, ['person', 'group']):
            return findAndTombstoneAdminProfile(profileId, groupId, tombstone, done)

          // if the profile was a person/group, we also want to check for a
          // linked admin profile and delete that too
          case isEqual(typePath, ['person', 'admin']):
            return findAndTombstoneGroupProfile(profileId, tombstone, done)

          default:
            done()
        }
      })
    })
  }
}

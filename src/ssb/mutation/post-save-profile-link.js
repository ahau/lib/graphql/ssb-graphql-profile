const { GraphQLError } = require('graphql')

module.exports = function PostSaveProfileLink (sbot) {
  return function postSaveProfileLink (input, cb) {
    if (input.id) {
      // TODO make updates possible
      // TODO check permissions?
      return cb(new GraphQLError('updates of profile links not yet supported'))
    }

    const details = Object.assign(
      input,
      { type: (input.group) ? 'group-profile' : 'feed-profile' }
    )

    sbot.profile.link.create(details, (err, link) => {
      if (err) cb(new GraphQLError(err))
      else cb(null, link.key)
    })
  }
}

const { promisify: p } = require('util')

const GetProfile = require('./query/get-profile')
// const GetPersons = require('./query/get-persons')
const GetTribes = require('./query/get-tribes')
const GetSubtribes = require('./query/get-subtribes')
const GetTribe = require('./query/get-tribe')
const GetConnectedPeers = require('./query/get-connected-peers')
const GetPatakas = require('./query/get-patakas')
const GetTiaki = require('./query/get-tiaki')
const GetAdminProfile = require('./query/get-admin-profile')

const FindPersons = require('./query/find-persons')
const ListPerson = require('./query/list-person')

const PostSavePerson = require('./mutation/post-save-person')
const PostTombstonePerson = require('./mutation/post-tombstone-person')
const PostSaveCommunity = require('./mutation/post-save-community')
const PostSavePataka = require('./mutation/post-save-pataka')
const PostSaveProfileLink = require('./mutation/post-save-profile-link')

const buildCache = require('./cache')

module.exports = function (ssb) {
  const cache = buildCache(ssb)

  const getProfile = GetProfile(ssb, cache.profile)
  const getTribe = GetTribe(ssb, getProfile)

  const getTiaki = GetTiaki(ssb)
  const getPatakas = GetPatakas(ssb, getProfile)

  return {
    ...promisifyAll({
      getProfile,
      getTribe,
      getTiaki,

      // getPersons: GetPersons(ssb, getProfile),
      getTribes: GetTribes(ssb, getTribe),
      getSubtribes: GetSubtribes(ssb, getTribe),
      getConnectedPeers: GetConnectedPeers(ssb, getProfile),
      getPatakas: (cb) => getPatakas(2, cb),
      findPersons: FindPersons(ssb),
      listPerson: ListPerson(ssb, cache.profile),

      postSavePerson: PostSavePerson(ssb),
      postTombstonePerson: PostTombstonePerson(ssb),
      postSaveCommunity: PostSaveCommunity(ssb),
      postSavePataka: PostSavePataka(ssb),
      postSaveProfileLink: PostSaveProfileLink(ssb)
    }),
    getAdminProfile: GetAdminProfile(ssb, getProfile, cache.adminLink),

    gettersWithCache: {
      getProfile,
      getTiaki
    }
  }
}

function promisifyAll (obj) {
  Object.entries(obj).forEach(([key, callbackFn]) => {
    obj[key] = p(callbackFn)
  })
  return obj
}

const get = require('lodash.get')
const pick = require('lodash.pick')

// this maps graphql inputs into the details that crut modules expect
// the `tombstone` field needs particular attention
module.exports = {
  person (input) {
    const output = {}

    Object.entries(input).forEach(([key, value]) => {
      switch (key) {
        // output never include type/ id
        case 'type':
        case 'id': return

        case 'altNames':
          output[key] = mapSimpleSet(value)
          return

        case 'customFields':
          output[key] = {}
          value.forEach(field => {
            if (get(field, 'type') === 'date') {
              field.value = {
                type: 'date',
                value: field.value
              }
            }

            output[key][field.key] = field.value // difference from community.customFields below
            delete output[key][field.key].key
          })
          return

        default:
          output[key] = mapCommonField(key, value)
      }
    })

    return output
  },
  community (input) {
    const output = {}

    Object.entries(input).forEach(([key, value]) => {
      switch (key) {
        // output never include type/ id
        case 'type':
        case 'id': return

        case 'customFields':
          output[key] = {}
          value.forEach(field => {
            output[key][field.key] = field // difference from person.customFields above

            if (field.tombstone) {
              output[key][field.key].tombstone = mapCommonField('tombstone', field.tombstone)
            }

            delete output[key][field.key].key
          })
          return

        default:
          output[key] = mapCommonField(key, value)
      }
    })

    return output
  },
  pataka (input) {
    const output = {}

    Object.entries(input).forEach(([key, value]) => {
      switch (key) {
        // output never include type/ id
        case 'type':
        case 'id': return

        default:
          output[key] = mapCommonField(key, value)
      }
    })

    return output
  }
}

function mapCommonField (key, value) {
  let output

  switch (key) {
    case 'authors':
      output = mapSimpleSet(value)
      break

    case 'avatarImage':
    case 'headerImage':
      output = pick(value, ['blob', 'unbox', 'mimeType', 'size', 'width', 'height'])
      break

    case 'tombstone':
      output = {}
      output = pick(value, ['date', 'reason'])
      output.date = Number(output.date) || Date.now()
      // graphql only allows 32bit signed Ints
      // so we're passing a Date and converting it to Int for ssb
      break
    default:
      output = value
  }

  return output
}

function mapSimpleSet (value) {
  let output = {}

  // sometimes passing {} into graphql converts it into undefined
  if (value === undefined) value = {}

  if (value.add && value.add.length) output.add = value.add
  if (value.remove && value.remove.length) output.remove = value.remove

  if (Object.keys(output).length === 0) output = undefined

  return output
}

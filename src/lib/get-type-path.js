module.exports = function getTypePath (typeStr, recps) {
  const typePath = typeStr.split('/').filter(t => t !== 'profile')

  if (typePath.length === 1) {
    typePath.push(recps ? 'group' : 'public')
  }

  return typePath
}

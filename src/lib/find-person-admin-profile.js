const { GraphQLError } = require('graphql')

module.exports = function FindPersonAdminProfile (ssb) {
  return function findAdminProfile (profileId, groupId, cb) {
    // first, check if there is a direct link/profile-profile/admin
    ssb.profile.person.group.findAdminProfileLinks(profileId, (err, links) => {
      if (err) return cb(new GraphQLError(err))

      if (links.parentLinks.length) {
        if (links.parentLinks.length > 1) console.error('expected 1 admin profile, found', links.parentLinks.length)

        const adminProfileId = links.parentLinks[0].parent
        return ssb.profile.person.admin.get(adminProfileId, cb)
      }

      // second, look to see if there are links:
      //   (groupProfile) <--link-- (feedId) --link--> (adminProfile)

      // 1. find adminGroupId from groupId
      // 2. find adminGroups poBoxId (or fall back to adminGroupId)
      // 3. find feedId associated with profileId
      // 4. find a profileId associated with feedId (3) AND poBoxId/adminGroupId (2)
      ssb.tribes.findSubGroupLinks(groupId, (err, links) => {
        if (err) return cb(new GraphQLError(err))
        const adminLinks = links.filter(link => link.admin)

        if (!adminLinks.length) return cb(null, null) // we're not part of admin group
        if (adminLinks.length > 1) console.error(`found ${adminLinks.length} adminLinks for ${groupId}`)

        const adminGroupId = adminLinks[0].subGroupId
        ssb.tribes.poBox.get(adminGroupId, (err, data) => {
          if (err && !err.message.match(/no poBox found/)) return cb(new GraphQLError(err))

          const recpId = data?.poBoxId || adminGroupId
          ssb.profile.findFeedsByProfile(profileId, { selfLinkOnly: false }, (err, results) => {
            const feeds = unique([...results.self, ...results.other])

            if (err) return cb(new GraphQLError(err))
            if (!feeds.length) return cb(null, null) // might be an unowned profile?
            if (new Set(feeds).size > 1) console.error(`found ${new Set(feeds).size} different feeds that might own ${profileId}`)
            // TODO this should perhaps be an cb(error)

            const feedId = feeds[0]
            ssb.profile.findByFeedId(feedId, { groupId: recpId, selfLinkOnly: false }, (err, profiles) => {
              if (err) return cb(new GraphQLError(err))

              const profile = [...profiles.self.private, ...profiles.other.private]?.[0]
              cb(null, profile) // may be undefined
            })
          })
        })
      })
    })
  }
}

// preserves order (not sure if Set would?)
function unique (arr) {
  return arr.reduce((acc, id) => {
    if (!acc.includes(id)) acc.push(id)
    return acc
  }, [])
}

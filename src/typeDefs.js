const gql = require('graphql-tag')

module.exports = gql`
  scalar CustomField

  extend type Query {
    """
    Get every message of type profile/community and present a list of Communities
    """
    tribes: [TribeFaces]

    """
    Get subgroups by tribe id
    """
    subtribes(id: String!): [SubtribeFaces]

    """
    Get every profile for a particular group
    """
    tribe(id: String!): TribeFaces

    """
    Returns information about the profile/person associated with the Person of given id
    """
    person(id: String!): Person

    """
    Returns information about the profile/community associated with the Community of given id
    """
    community(id: String!): Community

    """
    Returns information about the profile/pataka
    """
    pataka(id: String!): Pataka

    """
    Returns information about the profile associated with the Profile of given id
    """
    profile(id: String!): Profile

    """
    Get the person or pataka profiles of peers you are current connected to (requires ssb-conn)
    """
    connectedPeers: ConnectedPeers

    """
    Get get a list of patakas which will currently replicate you (are within 2 hops)
    """
    patakas: [Pataka]

    """
    Returns profiles which match the given name
    """
    findPersons(name: String!, type: String, groupId: String): [Person]

    """
    List person profiles of a particular type (in a particular group)
    """
    listPerson(type: String!, groupId: String): [Person]
  }

  extend type Mutation {
    """
    Create (or update) a person profile
    """
    savePerson(input: PersonProfileInput!): String
    """
    delete a person profile
    """
    deletePerson(id: String!, tombstoneInput: TombstoneInput): String!

    """
    Create (or update) a community profile
    """
    saveCommunity(input: CommunityProfileInput!): String

    """
    Create (or update) a pataka profile
    """
    savePataka(input: PatakaProfileInput!): String

    """
    Create (or update) a link between group and a profile
    """
    saveGroupProfileLink(input: GroupProfileLinkInput!): String

    """
    Create (or update) a link between **your feed** and a profile
    """
    saveFeedProfileLink(input: FeedProfileLinkInput!): String
  }

  input PersonProfileInput {
    id: String
    type: String

    preferredName: String
    legalName: String
    altNames: SetInput

    avatarImage: ImageInput
    headerImage: ImageInput
    description: String
    gender: Gender
    source: ProfileSource
    aliveInterval: EdtfInterval
    deceased: Boolean
    placeOfBirth: String
    placeOfDeath: String
    buriedLocation: String
    birthOrder: Int
    tombstone: TombstoneInput
    
    city: String
    country: String
    postCode: String
    profession: String

    education: [String]
    school: [String]

    # person.admin
    email: String
    phone: String
    address: String

    customFields: [PersonCustomFieldInput]

    authors: SetInput
    recps: [String]
    allowPublic: Boolean
  }

  input CommunityProfileInput {
    id: String
    type: String

    preferredName: String
    description: String

    avatarImage: ImageInput
    headerImage: ImageInput
    
    tombstone: TombstoneInput
    
    address: String
    city: String
    country: String
    postCode: String
    email: String
    phone: String

    # community.public
    joiningQuestions: [CustomFormFieldInput]
    poBoxId: String
    customFields: [CommunityCustomFieldInput]
    acceptsVerifiedCredentials: Boolean
    issuesVerifiedCredentials: Boolean

    # community.group settings
    allowWhakapapaViews: Boolean
    allowPersonsList: Boolean
    allowStories: Boolean
    

    authors: SetInput
    recps: [String]
    allowPublic: Boolean
  }

  input PatakaProfileInput {
    id: ID!

    preferredName: String
    description: String

    avatarImage: ImageInput
    headerImage: ImageInput
    
    address: String
    city: String
    country: String
    postCode: String
    phone: String
    email: String
  
    tombstone: TombstoneInput
    isBlocked: Boolean
  }

  """
  Input for linking a group to a profile
  """
  input GroupProfileLinkInput {
    id: String

    group: String!
    profile: String!
    allowPublic: Boolean
    parentGroupId: String
  }

  """
  Input for linking **your feed** and a profile
  """
  input FeedProfileLinkInput {
    id: String

    profile: String!
    allowPublic: Boolean
  }

  """
  Input for adding or removing an set item.
  """
  input SetInput {
    add: [String]
    remove: [String]
  }

  """
  Input for creating a ssb-profile image
  """
  input ImageInput {
    blob: ID!
    mimeType: String!
    size: Int
    unbox: String
    width: Int
    height: Int
  }

  extend type PublicIdentity @key(fields: "feedId") {
    profile: Person
  }
  extend type PersonalIdentity @key(fields: "groupId") {
    profile: Person
  }

  """
  Profile Image fields for the ssb-profile plugin
  """
  type Image {
    blob: ID
    mimeType: String
    size: Int
    width: Int
    height: Int
    uri: String
  }

  """
  Gender of a profile
  """
  enum Gender {
    male
    female
    other
    unknown
  }

  """
  Source of a profile, where it came from
  """
  enum ProfileSource {
    ahau
    webForm
  }

  interface Profile {
    id: ID!
    type: String
    recps: [String]

    preferredName: String
    description: String

    avatarImage: Image
    headerImage: Image
    
    address: String
    city: String
    country: String
    postCode: String
    phone: String
    email: String
  
    tombstone: Tombstone
  }
  
  """
  Person fields used by ssb-profile plugin
  """
  type Person implements Profile {
    id: ID!    
    type: String
    recps: [String]
    
    preferredName: String
    legalName: String
    altNames: [String]
    description: String
    
    avatarImage: Image
    headerImage: Image
    
    gender: Gender
    source: ProfileSource
    aliveInterval: EdtfInterval
    birthOrder: Int
    deceased: Boolean
    placeOfBirth: String
    placeOfDeath: String
    buriedLocation: String

    address: String
    city: String
    country: String
    postCode: String
    phone: String
    email: String
    profession: String

    education: [String]
    school: [String]

    tombstone: Tombstone

    # additional fields
    adminProfile: Person
    customFields: [PersonCustomFieldInterface]
  }

  type Pataka implements Profile {
    id: ID!
    feedId: String!,
    type: String
    recps: [String]

    preferredName: String
    description: String

    avatarImage: Image
    headerImage: Image
    
    address: String
    city: String
    country: String
    postCode: String
    phone: String
    email: String
  
    tombstone: Tombstone
    isBlocked: Boolean!
  }
  
  """
  The public and private facing profiles for a particular tribe (group). These have been linked to the group via the saveGroupProfileLink api.
  """
  type TribeFaces {
    id: ID!
    public: [Community]
    private: [Community]
    subtribes: [SubtribeFaces]
  }

  type SubtribeFaces {
    id: ID!
    public: [Community]
    private: [Community]
  }

  """
  Community profile (fields from ssb-profile plugin)
  """
  type Community implements Profile {
    id: ID!
    type: String
    recps: [String]
    
    preferredName: String
    description: String

    avatarImage: Image
    headerImage: Image

    address: String
    city: String
    country: String
    postCode: String
    phone: String
    email: String

    # private settings
    allowWhakapapaViews: Boolean
    allowPersonsList: Boolean
    allowStories: Boolean

    # public settings
    acceptsVerifiedCredentials: Boolean
    issuesVerifiedCredentials: Boolean
    joiningQuestions: [CustomFormField]
    customFields: [CommunityCustomFieldInterface]

    poBoxId: String

    tombstone: Tombstone
  }

  interface CommunityCustomFieldInterface {
    key: String!
    type: CustomFieldType!
    label: String!
    required: Boolean!
    visibleBy: String!

    # optional
    order: Float
    tombstone: Tombstone
  }

  type CommunityCustomField implements CommunityCustomFieldInterface {
    key: String!
    type: CustomFieldType!
    label: String!
    required: Boolean!
    visibleBy: String!

    # optional
    order: Float
    tombstone: Tombstone
  }

  type CommunityCustomFieldList implements CommunityCustomFieldInterface {
    key: String!
    type: CustomFieldType!
    label: String!
    required: Boolean!
    visibleBy: String!

    # optional
    order: Float

    # only for type list
    options: [String]
    multiple: Boolean

    # other
    tombstone: Tombstone
  }

  type CommunityCustomFieldFile implements CommunityCustomFieldInterface {
    key: String!
    type: CustomFieldType!
    label: String!
    required: Boolean!
    visibleBy: String!

    # optional
    order: Float

    # only for type file
    multiple: Boolean
    fileTypes: [CustomFieldFileType]
    description: String

    # other
    tombstone: Tombstone
  }

  type CustomFormField {
    type: CustomFormFieldType
    label: String
  }

  interface PersonCustomFieldInterface {
    key: String!
    value: CustomField
  }

  type PersonCustomField implements PersonCustomFieldInterface {
    key: String!
    value: CustomField
  }

  type PersonCustomFieldDate implements PersonCustomFieldInterface {
    key: String!
    type: CustomFieldType! # optional, for now only used for custom field dates
    value: CustomField
  }

  input CustomFormFieldInput {
    type: CustomFormFieldType
    label: String
  }

  input CommunityCustomFieldInput {
    key: String!
    type: CustomFieldType!
    label: String!
    required: Boolean!
    visibleBy: String!

    # optional
    order: Float

    # only for type list
    options: [String]

    # only for type file
    fileTypes: [CustomFieldFileType]
    description: String

    # only for type list and file
    multiple: Boolean


    # other
    tombstone: TombstoneInput
  }

  input PersonCustomFieldInput {
    key: String!
    type: String # optional, for now only used for custom field dates
    value: CustomField
  }

  enum CustomFormFieldType {
    input
    textarea
  }

  enum CustomFieldType {
    text
    list
    array
    checkbox
    number
    date
    file
  }

  enum CustomFieldFileType {
    document
    photo
    video
    audio
  }

  type ConnectedPeers {
    person: [Person]
    pataka: [Pataka]
  }

`

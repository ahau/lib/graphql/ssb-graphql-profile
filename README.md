# @ssb-graphql/profile

GraphQL types and resolvers for Secure Scuttlebutt profile plugin (ssb-profile)

## Usage

Install ssb-graphql's main and profile packages:

`npm i -S @ssb-graphql/main @ssb-graphql/profile`

## Example Usage

```js
const ahauServer = require('ahau-graphql-server')

const ssbProfileGraphql = require('ssb-profile-graphql')
const Server = require('ssb-server')
const Config = require('ssb-config/inject')

const config = Config({})

const sbot = Server
  .use(require('ssb-backlinks'))
  .use(require('ssb-query'))
  .use(require('ssb-profile'))
  .call(null, config)

const main = require('@ssb-graphql/main')(sbot)
const profile = require('@ssb-graphql/profile')(sbot)

main.loadContext((err, context) => {
  const server = new ahauServer({
    schemas: [
      main,
      profile // { typeDefs, resolvers }
              // ... add more here
    ],
    context
  })

  // ... 
})
```

## API

Things provided by instantiating `@ssb-graphql/profile`:
- `typeDefs`
- `resolvers`
- `gettersWithCache`: an object with _callback-style_ getters (currently for `getProfile`

In general, this module adds functionality:
- `ssb-profile` methods
  - create profiles (person, community)
  - read profiles
  - create links: feed-profile, group-profile
  - find profiles by a name
- resolve further profile details on the `whoami` query (`@ssb-graphql/main`)
- get all tribes (community) profiles
- get all peers you're within range of (profiles grouped by type)
- get all peers you're currented connected to (profiles grouped by type)

See `/src/typeDefs.js` for the most up to date definition of what's offered here.

### `query { findPersons(name: String!, type: String, groupId: String ): [Profile] }

- `name` (required) a string of minimum length 3, a (partial) fragment of a name
- `type` can be:
    - *String*
        - `person` = public + group person profiles (default)
        - `person/admin` = admin person profiles
        - `person/source` = source person profiles
        - `community` = public + group community profiles
        - `pataka` = public pataka profiles
    - *null* - this means "get me all matches of any type"
- `groupId` *String* - narrows to profiles only in `groupId` (can be a GroupId or POBoxId)

### `mutation { saveProfile(input) }: String`

This is a complex endpoint which does A LOT of work.

#### case: create profile

If no `id` is passed in, it is assumed that you are creating a profile.
The exact type of profile is then determined by `type` and `recps`

IF you fall into the person-group type (type: `person`, recps: `[groupId]`) AND there are details
that are for the person-admin fields (e.g. `phone`), then this method will attempt to locate an admin
subgroup, and publish and link up a profile there.
ONLY the admin-only fields will be saved to the `profile/person/admin` profile (as all the group fields are already
saved to the group profile)

#### case: updating profile

If an `id` is passed in, then this method will attempt to update the given profile, and handle other assumed
desired updates.

If the profile is one of "your profiles" (linked to your feedId), then a "bulk-update" of all your linked profiles
is triggered (saving the appropriate details to each, e.g. phone only to admin-profiles).
_(The only exception to this is if you happen to be calling an update on your public profile - this does not bulk-update other profiles. The reason for this is that we want to allow people to have a different external facing name for security)_

If the profile isn't "yours", then this method will update the profile as best it can, and overflow details it can't save to a "group profile" into an "admin profile" (if it can).
- if it's a profile that anyone can edit, then you update that
   - if there are admin-only details, then those details are published to a linked admin-only profile (if you're an admin)
- if you are not allowed to edit the profile, then all details are saved to a linked admin-only profile (assuming you're an admin)

---

## Requirements
An `ssb-server` with the following plugins :
- `ssb-backlinks`
- `ssb-query`
- `ssb-profile`

Note if running `@ssb-graphql/main`, that also has requirements


